//
//  SceneDelegate.swift
//  siemob
//
//  Created by Saroufim, Jessica on 28.10.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import UIKit
import SwiftUI
import Prototyper
import TouchVisualizer

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyrboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

        // Create the SwiftUI view that provides the window contents.

        // Use a UIHostingController as window root view controller.
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let model = Model()
            let entryView = InitialView().environmentObject(model).environmentObject(MultipeerHelper()).environmentObject(ARGlobalState())

            window.rootViewController = UIHostingController(rootView: entryView)
            self.window = window
            
            PrototyperController.showFeedbackButton = false
            Visualizer.start()
            
            
            window.makeKeyAndVisible()
        }
    }
}
