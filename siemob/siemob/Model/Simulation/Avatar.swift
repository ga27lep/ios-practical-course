//
//  Avatar.swift
//  siemob
//
//  Created by Simon Dabrowski on 16.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import RealityKit
import AVFoundation
import Combine

public class Avatar {
   // private var arView: WorldView = WorldView(frame: .zero)

    private let scale = Float(0.007)
    public var anchorEntity: AnchorEntity
    public var animationDescription: (name: String, nr: Int)
    public var objectLastMovedTo: AnchorEntity?
    public var objects:  [(object: (anchor: AnchorEntity, position: SIMD3<Float>), index: Int)]
    public var radioObjectIndex: Int
    public var gymnasticIndex: Int
    var animations = Animation().animations
    var addParent: Bool = false
    private var parentAnchor = AnchorEntity()
    private var parentExist: Bool = false
    private var audioSound: Bool = false
    private var audioPlayer: AVAudioPlayer?
    private var audioPlayer2: AVAudioPlayer?
    public var simulationStarted: Bool
  //  public var parentList: [AnchorEntity]
    public var playOnce: Int = 1
    public var musicBox: (object: (anchor: AnchorEntity, position: SIMD3<Float>) , index: Int)?
    private var requestCancellable: AnyCancellable?
    private var musicB: Bool = true
    
    public init(anchor: AnchorEntity, objects:  [(object: (anchor: AnchorEntity, position: SIMD3<Float>), index: Int)], radioObjectIndex: Int,
                simulationStarted: Bool, gymnasticIndex: Int, playOnce: Int ) {
        self.anchorEntity = anchor
        self.objects = objects
        self.animationDescription = (name: "", nr: 1)
        self.radioObjectIndex = radioObjectIndex
        self.simulationStarted = simulationStarted
        self.gymnasticIndex = gymnasticIndex
      //  self.parentList = parentList
        self.playOnce = playOnce
       // self.musicBox = musicBox as! (object: (anchor: AnchorEntity, position: SIMD3<Float>), index: Int)
        
        let randomAnimationIndex = Int.random(in: 0 ..< self.animations.count - 1); print(randomAnimationIndex)
        let name = self.animations.first(where: { randomAnimationIndex == $0.animation.index })!.animation.obj.name
        let animationURLToAdd = self.animations.first(where: { name == $0.animationDescription.name })?.animation.obj.resourceURL
        let animationToAdd = self.animations.first(where: { name == $0.animationDescription.name })?.animation.obj
        let animationDescription = self.animations.first(where: { name == $0.animationDescription.name })?.animationDescription
        
        
        if let animationURLToAdd = animationURLToAdd, let animationDescription = animationDescription {
            
            requestCancellable = Entity.loadAsync(contentsOf: animationURLToAdd)
            .sink(receiveCompletion: { loadCompletion in
                return
            }, receiveValue: { entity in
                entity.scale = [self.scale, self.scale, self.scale]
                
                let parentEntity = ModelEntity()
                parentEntity.addChild(entity)
                self.anchorEntity.addChild(parentEntity)
                
                self.animationDescription = animationDescription
                
                entity.availableAnimations.forEach {
                    entity.playAnimation($0.repeat())
                }
                self.continouslyAnimate()
            })
        }
    }
    
    
    public init(anchor: AnchorEntity, objects:  [(object: (anchor: AnchorEntity, position: SIMD3<Float>), index: Int)], radioObjectIndex: Int,
                simulationStarted: Bool, gymnasticIndex: Int, playOnce: Int,  musicBox: (object: (anchor: AnchorEntity, position: SIMD3<Float>) , index: Int)) {
        self.anchorEntity = anchor
        self.objects = objects
        self.animationDescription = (name: "", nr: 1)
        self.radioObjectIndex = radioObjectIndex
        self.simulationStarted = simulationStarted
        self.gymnasticIndex = gymnasticIndex
      //  self.parentList = parentList
        self.playOnce = playOnce
        self.musicBox = musicBox
        
        let randomAnimationIndex = Int.random(in: 0 ..< self.animations.count - 1); print(randomAnimationIndex)
        let name = self.animations.first(where: { randomAnimationIndex == $0.animation.index })!.animation.obj.name
        let animationURLToAdd = self.animations.first(where: { name == $0.animationDescription.name })?.animation.obj.resourceURL
        let animationToAdd = self.animations.first(where: { name == $0.animationDescription.name })?.animation.obj
        let animationDescription = self.animations.first(where: { name == $0.animationDescription.name })?.animationDescription
        
        
        if let animationURLToAdd = animationURLToAdd, let animationDescription = animationDescription {
            
            requestCancellable = Entity.loadAsync(contentsOf: animationURLToAdd)
            .sink(receiveCompletion: { loadCompletion in
                return
            }, receiveValue: { entity in
                entity.scale = [self.scale, self.scale, self.scale]
                
                let parentEntity = ModelEntity()
                parentEntity.addChild(entity)
                self.anchorEntity.addChild(parentEntity)
                
                self.animationDescription = animationDescription
                
                entity.availableAnimations.forEach {
                    entity.playAnimation($0.repeat())
                }
                self.continouslyAnimate()
            })
        }
    }
    
    /**
     This function should always be called again when one Sequence of steps (animations) is finished.
     Every sequence has to call this function when it  finished.
     At the moment the only sequence that exists and therefore is called is moveToObject()
     */
    public func continouslyAnimate() {
        if objects.isEmpty {
            return
        }
        if simulationStarted {
        moveToObject()
        }
    }
    
    public func moveToObject() {
        //if the object is the same as the one he already is at / moves towards we do not have to do anything
      
        let objectToMoveTo = objects.randomElement()
        guard var object = objectToMoveTo?.object.anchor, var objectIndex = objectToMoveTo?.index else {
            print("ERROR: No objects where placed and therefore we can't simulate the characters")
            return
        }
        if let musicBox = self.musicBox {
            if self.musicB == true {
                object = musicBox.object.anchor
                objectIndex = musicBox.index
                self.musicB = false
            }
        }
       
        if self.objectLastMovedTo == object {
            print("Avatar doesn't move because it is the same object as last time")
            var waitFor = Double.random(in: 5 ..< 8)
            print("Avatar wait for: " + String(waitFor) + " seconds at object")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + waitFor) {
                print("Waited")
                self.continouslyAnimate()
            }
            return
        }
        
        self.objectLastMovedTo = object
        
        //Get the position of the avatar in relation to the object he will be moving towards
        let pos = self.anchorEntity.position(relativeTo: object)
        //orient the avatar according to the direction he will be walking towards
        self.anchorEntity.look(at: object.position, from: pos, relativeTo: object)
        //flip the avatar 180 degress so that his front and not his back faces the walking direction
        self.anchorEntity.setOrientation(simd_quatf(angle: .pi, axis: [0, 1, 0 ]), relativeTo: self.anchorEntity)
        //set the destinations rotation to the same orientation as the start
        let orientation = self.anchorEntity.orientation(relativeTo: object)
        var newPos = object.transform
        newPos.rotation = orientation
        
        let distnaceToObject = Float(0.15)
        var newX = distnaceToObject
        var newZ = distnaceToObject
        if pos.x > 0 {
            newX = -distnaceToObject
        }
        if pos.z > 0 {
            newZ = -distnaceToObject
        }
        newPos.translation -= simd_float3(x: newX, y: 0, z: newZ)
        
        //calculate the duration the avatar needs for the way
        let duration = calculateDuration(object: object)
        
        //change the animation to walking
        self.startWalking()
        
        //Change the animation after he arrived
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            //Should fix so that the animation isn't changed while walking between objects when the Avatar did not arrive yet
            let distance = self.calculateDistance(object: object)
            if distance <= 6 {
                if objectIndex == self.gymnasticIndex {
                    let newAnimation = self.chooseCryingAnimation()
                    let sound = Bundle.main.url(forResource: "lidcreak", withExtension: "mp3") ?? URL(fileURLWithPath: "")
                    do { try self.audioPlayer = AVAudioPlayer(contentsOf: sound) } catch { print("Could not load entity asset"); return
                    }
                    if let audioPlayer = self.audioPlayer { if !audioPlayer.isPlaying { self.audioPlayer?.play()}}
                    self.replaceAnimation(animation: newAnimation)
                    let crying = Bundle.main.url(forResource: "crying", withExtension: "mp3") ?? URL(fileURLWithPath: "")
                    do { try self.audioPlayer2 = AVAudioPlayer(contentsOf: crying) } catch { print("Could not load entity asset"); return
                    }
                    if let audioPlayer2 = self.audioPlayer2 { if !audioPlayer2.isPlaying { self.audioPlayer2?.play(); self.audioPlayer2?.volume = 5} }
                } else
                    if objectIndex == self.radioObjectIndex && self.playOnce == 1 {
                        print("playOnce", self.playOnce)
                    self.playOnce += 1
                    let newAnimation = self.chooseDanceAnimation()
                    let sound = Bundle.main.url(forResource: "Wild_Pogo", withExtension: "mp3") ?? URL(fileURLWithPath: "")
                               do { try self.audioPlayer = AVAudioPlayer(contentsOf: sound) } catch { print("Could not load entity asset"); return}
                    if let audioPlayer = self.audioPlayer { if audioPlayer.isPlaying == false { self.audioPlayer?.play() } }
                    self.replaceAnimation(animation: newAnimation)
                    }  else {
                    let newAnimation = self.chooseNextAnimation(); self.replaceAnimation(animation: newAnimation)
//                    if self.addParent == true {
//                        self.loadParent(); self.addParent = false; self.parentExist = true
//                    }
                }
            }
            let waitFor = Double.random(in: 4 ..< 6 )
            print("Avatar wait for: " + String(waitFor) + " seconds at object")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + waitFor) {
               // print("Waited")
               // if self.parentExist { self.arView.scene.removeAnchor(self.parentAnchor); self.parentExist = false }
               // self.audioSound = false
                self.audioPlayer?.stop(); self.audioPlayer2?.stop()
                self.continouslyAnimate()
            }
        }
        
        //make the avatar move towards the object
        print("Avatar move for: " + String(duration) + " seconds")
        self.anchorEntity.move(to: newPos, relativeTo: object.anchor, duration: duration, timingFunction: .linear)
    }
    
    private func chooseCryingAnimation() -> URL {
        let newAnimation: URL
               switch animationDescription.name {
               case "jasper":
                   newAnimation = Bundle.main.url(forResource: "TerrifiedMaleNew", withExtension: "usdz") ?? URL(fileURLWithPath: "")
               default:
                   newAnimation = Bundle.main.url(forResource: "CryingNew", withExtension: "usdz") ?? URL(fileURLWithPath: "")
               }
               return newAnimation
    }
    
//    private func loadParent() {
//        var pos = self.anchorEntity.position
//        pos.x -= 0.09
//        pos.z -= 0.09
//        self.parentAnchor = AnchorEntity(world: pos)
//        let parentEntity: Entity
//        let parentURL = Bundle.main.url(forResource: "Yelling", withExtension: "usdz") ?? URL(fileURLWithPath: "")
//        do {
//            try parentEntity = Entity.load(contentsOf: parentURL)
//        } catch {
//            print("Could not load entity asset"); return
//        }
//        parentEntity.scale = [self.scale, self.scale, self.scale]
//
//        let parentOfParentEntity = ModelEntity()
//        parentOfParentEntity.addChild(parentEntity)
//        self.parentAnchor.addChild(parentEntity)
//       // arView.scene.anchors.append(self.parentAnchor)
//        parentEntity.availableAnimations.forEach {
//            parentEntity.playAnimation($0.repeat())
//        }
//    }
    
    private func chooseDanceAnimation() -> URL {
        let newAnimation: URL
        switch animationDescription.name {
        case "jasper":
            newAnimation = Bundle.main.url(forResource: "TwistDanceMaleNew", withExtension: "usdz") ?? URL(fileURLWithPath: "")
        default:
            newAnimation = Bundle.main.url(forResource: "BellydancingNew", withExtension: "usdz") ?? URL(fileURLWithPath: "")
        }
        return newAnimation
    }
    
    private func startWalking() {
                let randomIndex = Int.random(in: 0 ..< 3)
        print("randomindex", randomIndex)
                if randomIndex == 0 {
                    //insert walking animation here
                    let walking = animations.first(where: {
                               self.animationDescription.name == $0.animationDescription.name
                           })?.animation.obj.resourceURL
                           guard let walkingAnimation = walking else {
                               return
                           }
                           self.replaceAnimation(animation: walkingAnimation)
                } else {
                  //  insert running animation here
                    let running = animations.first(where: {self.animationDescription.name == $0.animationDescription.name &&
                        $0.animationDescription.nr == 5
                    })?.animation.obj.resourceURL
                    guard let runningAnimation = running else {
                        return
                    }
                    self.replaceAnimation(animation: runningAnimation)
                }
    }
    
    private func replaceAnimation(animation: URL) {
        DispatchQueue.main.async {
            let parentEntity = self.anchorEntity.children.first
            if let parentEntity = parentEntity {
                let simulation = parentEntity.children.first
                
                self.requestCancellable = Entity.loadAsync(contentsOf: animation)
                .sink(receiveCompletion: { loadCompletion in
                    print("Could not load entity asset")
                    return
                }, receiveValue: { entity in
                    if let simulation = simulation {
                        simulation.removeFromParent()
                    }
                    entity.scale = [self.scale, self.scale, self.scale]
                    entity.setParent(parentEntity)
                    entity.availableAnimations.forEach { entity.playAnimation($0.repeat()) }
                })
            }
        }
    }
    
    private func replaceAnimationForParent(animation: URL, parent: AnchorEntity) {
        let parentEntity = parent.children.first
                  if let parentEntity = parentEntity {
                      let simulation = parentEntity.children.first
                    
                    requestCancellable = Entity.loadAsync(contentsOf: animation)
                    .sink(receiveCompletion: { loadCompletion in
                        print("Could not load entity asset")
                        return
                    }, receiveValue: { entity in
                        
                        if let simulation = simulation {
                            simulation.removeFromParent()
                        }
                        entity.scale = [self.scale, self.scale, self.scale]
                        entity.setParent(parentEntity)
                        entity.availableAnimations.forEach { entity.playAnimation($0.repeat()) }
                    })
                  }
    }
    
//    private func moveToChild() {
//        parentList.forEach({ //replaceAnimationForParent(animation: Bundle.main.url(forResource: "Walking_Male_in_Place", withExtension: ".usdz") ?? URL(fileURLWithPath: ""), parent: $0)
//            $0.move(to: self.anchorEntity.transform, relativeTo: self.anchorEntity, duration: 3, timingFunction: .linear)
//           // replaceAnimationForParent(animation: Bundle.main.url(forResource: "Yelling", withExtension: ".usdz") ?? URL(fileURLWithPath: ""), parent: $0)
//        })
//         //self.anchorEntity.move(to: newPos, relativeTo: object.anchor, duration: duration, timingFunction: .linear)
//    }
    /**
     This function chooses the next animation to be shown
     The default animation is the standing still animation
     */
    private func chooseNextAnimation() -> URL {
        let animationDescriptionNew = (name: animationDescription.name, nr: animationDescription.nr + 1)
        self.animationDescription.nr += 1
        if self.animationDescription.nr == 3 {
            self.addParent = true
         //   moveToChild()
        }
        if self.animationDescription.nr > 4 {
            self.animationDescription.nr = 1
        }
        let newAnimation = animations.first(where: { self.animationDescription.name == $0.animationDescription.name &&
            self.animationDescription.nr == $0.animationDescription.nr})?.animation.obj.resourceURL
        if let newAnimation = newAnimation {
            return newAnimation
        }
        let walkingAnimation = animations.first(where: {animationDescriptionNew.name == $0.animationDescription.name})?.animation.obj.resourceURL
        if let walkingAnimation = walkingAnimation {
            self.animationDescription.nr = 0
            return walkingAnimation
        }
        
        return URL(fileURLWithPath: "")
    }
    
    private func calculateDuration(object: AnchorEntity) -> Double {
        return Double(3 * calculateDistance(object: object))
    }
    
    private func calculateDistance(object: AnchorEntity) -> Float {
        let objectPosition = self.anchorEntity.position(relativeTo: object)
        let calculatedDistnace = distance(object.transform.translation, objectPosition)
        if calculatedDistnace.isNaN {
            return 0.0
        }
        return calculatedDistnace
    }
    
    public func setRadioObjectIndex(newIndex: Int) {
        self.radioObjectIndex = newIndex
    }
}
