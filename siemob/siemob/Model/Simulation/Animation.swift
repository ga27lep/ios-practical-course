//
//  Animation.swift
//  siemob
//
//  Created by Lacatusu, Ana-Maria on 16.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation

public class Animation: PlaygroundItem {
    
    public var animations:
        [(animation: (obj: PlaygroundObject, index: Int), animationDescription: (name: String, nr: Int))] =
        [((PlaygroundObject(name: "jasper",
                            thumbnail: "jasper-thmb",
                            resource: "WalkingMaleNew", scale: 0.001), 0), ("jasper", 0)),
         
         ((PlaygroundObject(name: "jasper",
                            thumbnail: "jasper-thmb",
                            resource: "IdleMaleNew", scale: 0.001), 1), ("jasper", 1)),
         
         ((PlaygroundObject(name: "jasper",
                            thumbnail: "jasper-thmb",
                            resource: "JumpingNewMale", scale: 0.001), 2), ("jasper", 2)),
         
//         ((PlaygroundObject(name: "jasper",
//                            thumbnail: "jasper-thmb",
//                            resource: "Defeated", withExtension: "usdz")
//                                ?? URL(fileURLWithPath: ""), scale: 0.001), 3), ("jasper", 3)),
//
//         ((PlaygroundObject(name: "jasper",
//                            thumbnail: "jasper-thmb",
//                            resource: "InjuredWalk", withExtension: "usdz")
//                                ?? URL(fileURLWithPath: ""), scale: 0.001), 4), ("jasper", 4)),
         
//         ((PlaygroundObject(name: "jasper",
//                            thumbnail: "jasper-thmb",
//                            resource: "LookingAround", withExtension: "usdz")
//                                ?? URL(fileURLWithPath: ""), scale: 0.001), 5), ("jasper", 5)),
         ((PlaygroundObject(name: "jasper",
                            thumbnail: "jasper-thmb",
                            resource: "DefeatedNew", scale: 0.001), 3), ("jasper", 3)),
         
         ((PlaygroundObject(name: "jasper",
                            thumbnail: "jasper-thmb",
                            resource: "WavingMaleNew", scale: 0.001), 4), ("jasper", 4)),
         
          ((PlaygroundObject(name: "jasper",
         thumbnail: "jasper-thmb",
         resource: "RunMaleNew", scale: 0.001), 5), ("jasper", 5)),
         
         ((PlaygroundObject(name: "pearl",
                            thumbnail: "pearl-thmb",
                            resource: "Walking_femaleNew",
                            scale: 0.001), 6), ("pearl", 0)),
         
         ((PlaygroundObject(name: "pearl",
                            thumbnail: "pearl-thmb",
                            resource: "IdleFemaleNew",
                            scale: 0.001), 7), ("pearl", 1)),
         
         ((PlaygroundObject(name: "pearl",
                            thumbnail: "pearl-thmb",
                            resource: "Waving_femaleNew",
                            scale: 0.001), 8), ("pearl", 2)),
         
         ((PlaygroundObject(name: "pearl",
                            thumbnail: "pearl-thmb",
                            resource: "LayingPoseFemaleNew",
                            scale: 0.001), 9), ("pearl", 3)),
         
         ((PlaygroundObject(name: "pearl",
                            thumbnail: "pearl-thmb",
                            resource: "Waving_femaleNew",
                            scale: 0.001), 10), ("pearl", 4)),
         
//         ((PlaygroundObject(name: "pearl",
//                            thumbnail: "pearl-thmb",
//                            resource: "FemaleLayingPose2",
//                            scale: 0.001), 12), ("pearl", 5)),
         
         ((PlaygroundObject(name: "pearl",
                            thumbnail: "pearl-thmb",
                            resource: "RunFemaleNew",
                            scale: 0.001), 11), ("pearl", 5))]
         
//         ((PlaygroundObject(name: "pearl",
//                                   thumbnail: "pearl-thmb",
//                                   resource: "GoofyRunning",
//                                   scale: 0.001), 11), ("pearl", 5)),
         
//         ((PlaygroundObject(name: "jasper",
//                            thumbnail: "jasper-thmb",
//                            resource: "Yelling",
//                            scale: 0.001), 14), ("jasper", 7))]
    
    
    init() {
        super.init(name: "jasper",
                   thumbnail: "jasper-thmb",
                   resource: "Walking_Male_in_Place",
                   scale: 0.001)
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}
