//
//  Project.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import CoreData

@objc(Project)
public class Project: NSManagedObject {
    public var nameValue: String {
          return self.name ?? ""
      }

}

// MARK: - Extension: Storable
extension Project: Storable {
    static var entityName: String = "Project"
}

// MARK: - Extension: Identifiable
extension Project: Identifiable { }


// MARK: - Extension: Comparable
extension Project: Comparable {
    public static func < (lhs: Project, rhs: Project) -> Bool {
        guard let lhsName = lhs.name, let rhsName = rhs.name else {
            return true
        }
        return lhsName < rhsName
    }
}

extension Project {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Story> {
        return NSFetchRequest<Story>(entityName: "Project")
    }

    @NSManaged public var id: UUID
    @NSManaged public var name: String?

}

