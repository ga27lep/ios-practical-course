//
//  Server.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

extension Model {
    static var urlString: String = "http://ios1920siemob.ase.in.tum.de"
    
    fileprivate func handleResponseForLoginApiCall(_ data: Data, _ response: HTTPURLResponse, _ username: String) {
            switch response.statusCode {
            case 200:
                DispatchQueue.main.async {
                    self.authenticated = true
                    self.invalidLoginText = ""
                    let str = String(decoding: data, as: UTF8.self)
                    self.tokenStorage.storeToken(str)
                    self.loadProjectsFromServer()
                }
            case 400:
                DispatchQueue.main.async {
                    self.invalidLoginText = "User does not exist!!"
                }
            case 401:
                DispatchQueue.main.async {
                    self.invalidLoginText = "Invalid Password!!"
                }
            default:
                DispatchQueue.main.async {
                    self.invalidLoginText = "Error trying to log in!!"
                }
            }
    }
    
    public func loginAndRecieveToken(username: String, password: String) {
        let reqBody = ["name": username, "password": password]

        guard let httpBody = try? JSONSerialization.data(withJSONObject: reqBody, options: []) else {
            return
        }
        guard let url = URL(string: "\(Model.urlString)/login") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, _ in
            guard let data = data else {
                return
            }
            guard let response = response as? HTTPURLResponse else {
                return
            }
            self.handleResponseForLoginApiCall(data, response, username)
        }
        .resume()
    }
    
    public func loadProjectsFromServer() {
        guard let url = URL(string: "\(Model.urlString)/projects") else {
            return
        }
        var request = URLRequest(url: url)
        request.addValue("Bearer \(tokenStorage.loadToken())", forHTTPHeaderField: "Authorization")
        _ = URLSession.shared.dataTask(with: request) { data, _, _ in
            guard let data = data else {
                return
            }
            
            do {
                // Needed as the API does not conform to Json Standard
                var beginning = "{ \"projects\": "
                var str = String(decoding: data, as: UTF8.self)
                let end = "}"
                str.append(end)
                beginning.append(str)
                let dataNew = beginning.data(using: .utf8)!
                
                let serverResponse = try JSONDecoder().decode(AllProjectResponse.self, from: dataNew)
                DispatchQueue.main.async {
                    self.projects.removeAll()
                    serverResponse.projects.forEach({ project in
                        self.saveProject(id: UUID(uuidString: project.id), name: project.name)
                    })
                }
            } catch {
                DispatchQueue.main.async {
                    self.authenticated = false
                }
            }
        }
        .resume()
    }
    
    public func loadStoriesFromServer() {
         guard let url = URL(string: "\(Model.urlString)/projects/\(self.currentProjectID)/story") else {
             return
         }
         var request = URLRequest(url: url)
         request.addValue("Bearer \(tokenStorage.loadToken())", forHTTPHeaderField: "Authorization")
         _ = URLSession.shared.dataTask(with: request) { data, _, _ in
             guard let data = data else {
                 return
             }
             
             do {
                var beginning = "{ \"stories\": "
                var str = String(decoding: data, as: UTF8.self)
                let end = "}"
                str.append(end)
                beginning.append(str)
                let dataNew = beginning.data(using: .utf8)!
                
                 let serverResponse = try JSONDecoder().decode(AllStoryResponse.self, from: dataNew)
                 DispatchQueue.main.async {
                     self.stories.removeAll()
                        var orderCount = 5
                     serverResponse.stories.forEach({ story in
                        self.saveStory(id: nil, text: story.text, filledStory: story.story, order: orderCount)
                        orderCount -= 1
                     })
                 }
             } catch {
                 DispatchQueue.main.async {
                     self.authenticated = false
                 }
             }
         }
         .resume()
     }
    
    public func createStory(text: String, generate: Bool) {
        let accessPoint = "\(Model.urlString)/projects/\(self.currentProjectID)/story?generate=" + String(generate)
        guard let url = URL(string: accessPoint) else {
            return
        }
        let reqBody = ["text": text, "model": "774M", "length": "75"]
        guard let httpBody = try? JSONSerialization.data(withJSONObject: reqBody, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.addValue("Bearer \(tokenStorage.loadToken())", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        _ = URLSession.shared.dataTask(with: request) { data, _, _ in
            guard let data = data else {
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode(StoryResponse.self, from: data)
                DispatchQueue.main.async {
                    if self.stories.count >= 5 {
                        let removeID = self.stories.max()?.id
                        self.stories.removeAll {
                            $0.id == removeID
                        }
                    }
                    let newPos = (Int(self.stories.min()?.order ?? "0") ?? 0) + 1
                    self.saveStory(id: nil, text: serverResponse.text, filledStory: serverResponse.story, order: newPos)
                }
            } catch {
                DispatchQueue.main.async {
                    self.authenticated = false
                }
            }
        }
        .resume()
    }
    
    public func createProject(name: String) {
        guard let url = URL(string: "\(Model.urlString)/projects") else {
            return
        }
        let reqBody = ["name": name]
        guard let httpBody = try? JSONSerialization.data(withJSONObject: reqBody, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.addValue("Bearer \(tokenStorage.loadToken())", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        _ = URLSession.shared.dataTask(with: request) { data, _, _ in
            guard let data = data else {
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode(ProjectResponse.self, from: data)
                DispatchQueue.main.async {
                    self.saveProject(id: UUID(uuidString: serverResponse.id), name: serverResponse.name)
                    self.currentProjectID = serverResponse.id
                    self.loadStoriesFromServer()
                }
            } catch {
                DispatchQueue.main.async {
                    self.authenticated = false
                }
            }
        }
        .resume()
    }
}
