//
//  ItemLibrary.swift
//  siemob
//
//  Created by Yifeng Dong on 22.11.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

public class ItemLibrary {
    // @Published because if changed all views using it are refreshed.
    @Published public var items: [PlaygroundItem]
    
    //var polySearch = PolySearch() // (Google Poly)
    var searchCancellable: AnyCancellable?
    
    // MARK: - Initializers
    init(items: [PlaygroundItem]) {
        self.items = items
    }
    
    // MARK: - Find item by ID
    public func item(byId id: PlaygroundItem.ID?) -> PlaygroundItem? {
        for item in items where item.id == id {
            return item
        }
        return nil
    }
    
    // MARK: - Collection Enum
    public enum Collection: String, Identifiable, CaseIterable {
        case objects = "Objects"
        case actions = "Actions"
        case billboards = "Billboards"
        
        public var id: String {
            self.rawValue
        }
        
        public var icon: Image {
            switch self {
            case .objects:
                return Image(systemName: "house")
            case .actions:
                return Image(systemName: "person")
            case .billboards:
                return Image(systemName: "square.and.pencil")
            }
        }
    }
    
    // MARK: - Get subcategories
    public func items(collection: ItemLibrary.Collection) -> [PlaygroundItem] {
        switch collection {
        case .objects:
            return self.objects
        case .actions:
            return self.actions
        case .billboards:
            return self.billboards // TODO actions, recents
        }
    }
    
    // MARK: - Computed properties
    public var objects: [PlaygroundObject] {
        items.compactMap({ $0 as? PlaygroundObject })
    }
    
    public var actions: [RecordedAction] {
        items.compactMap({ $0 as? RecordedAction })
    }
    
    public var billboards: [PlaygroundItem] {
        items.compactMap({ $0 as? Annotation }) // For now
    }
    
    public func fillModelLibrary(newItem: PlaygroundItem) {
        self.items.append(newItem)
    }
}

// MARK: - Extension: ObservableObject
extension ItemLibrary: ObservableObject { }

extension ItemLibrary {
    public static let scale = Float(0.007);
    public static let mockLibrary = ItemLibrary(items: [
        Annotation.newAnnotation,
        RecordedAction.mock,
         
         PlaygroundObject(name: "MusicBox",
                          thumbnail: "gramophone",
                          resource: "gramophone",
                          scale: scale),
         PlaygroundObject(name: "Bench",
                          thumbnail: "bench_5_22-thmb",
                          resource: "Bench_5_12_2",
                          scale: scale),
         PlaygroundObject(name: "Bench",
                          thumbnail: "bench_5_27-thmb",
                          resource: "Bench_5_17_N",
                          scale: scale),
         PlaygroundObject(name: "Bench",
                          thumbnail: "bench_5_52-thmb",
                          resource: "Bench_5_52",
                          scale: scale),
         PlaygroundObject(name: "Bench",
                          thumbnail: "bench_5_82-thmb",
                          resource: "Bench_5_82",
                          scale: scale),
         PlaygroundObject(name: "Gymnastic",
                          thumbnail: "Gymnastic_Balance_Beam-thmb",
                          resource: "Gymnastic_Balance_Beam",
                          scale: scale),
         PlaygroundObject(name: "Ladder",
                          thumbnail: "ladder_09023-thmb",
                          resource: "ladder_09023",
                          scale: scale),
         PlaygroundObject(name: "Ladder",
                          thumbnail: "ladder_09024-thmb",
                          resource: "ladder_09024",
                          scale: scale),
         PlaygroundObject(name: "Sandbox",
                          thumbnail: "Sandbox_Marge_05004-thmb",
                          resource: "Sandbox_Marge_05004",
                          scale: scale),
         
         PlaygroundObject(name: "Sand",
                          thumbnail: "pile_of_sand-thmb",
                          resource: "pile_of_sand",
                          scale: scale),
          PlaygroundObject(name: "ATM",
                           thumbnail: "atm-thmb",
                           resource: "atm",
                           scale: (scale)),
         PlaygroundObject(name: "Seesaw",
                          thumbnail: "seasaw-thmb",
                          resource: "seasaw",
                          scale: 0.0015),
         PlaygroundObject(name: "Slide",
                          thumbnail: "slide-thmb",
                          resource: "slide",
                          scale: 0.002),
         PlaygroundObject(name: "Swing",
                          thumbnail: "swing-thmb",
                          resource: "cupic_swing",
                          scale: 0.004)
    ])
}
