//
//  RecordedAction.swift
//  siemob
//
//  Created by Yifeng Dong on 09.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import ARKit
import RealityKit

public final class RecordedAction: PlaygroundItem {
    var jointFrames: [[simd_float4x4]] = []
    var rootTranslations: [simd_float3] = []
    var rootRotations: [simd_quatf] = []
    

    #if targetEnvironment(simulator)
    #else

    override func load() -> Entity? {
        guard let resourceURL = resourceURL else { return nil }
        
        
        guard let loadedEntity = try? Entity.loadBodyTracked(contentsOf: resourceURL) else {
            return nil
        }
       
        // Scale the character to human size
        loadedEntity.scale = [1.0, 1.0, 1.0]
        
        return loadedEntity
    }
    #endif
    
    init(name: String,
         thumbnail: String? = "robot-thumb",
         resource: String? = "robot",
         scale: Float = 1.00,
         thumbnailURL: String? = "",
         id: String? = nil,
         jointFrames: [[simd_float4x4]]) {
        self.jointFrames = jointFrames
        super.init(name: name, thumbnail: thumbnail, resource: resource, scale: scale, thumbnailURL: thumbnailURL, id: id)
    }
     
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.jointFrames = try values.decode([[simd_float4x4]].self, forKey: .jointFrames)
        self.rootTranslations = try values.decode([simd_float3].self, forKey: .rootTranslations)
        self.rootRotations = try values.decode([simd_quatf].self, forKey: .rootRotations)
    }
    
    override public func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(jointFrames, forKey: .jointFrames)
        try container.encode(rootTranslations, forKey: .rootTranslations)
        try container.encode(rootRotations, forKey: .rootRotations)
    }
    
    private enum CodingKeys: String, CodingKey {
        case jointFrames, rootTranslations, rootRotations
    }
    
    func test() throws {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try encoder.encode(RecordedAction.mock)
        print(String(data: data, encoding: .utf8)!)
        
        let decoder = JSONDecoder()
        let ra = try decoder.decode(RecordedAction.self, from: data)
        print(ra.name)
    }
}

extension RecordedAction {
    static let mock = RecordedAction(name: "Action",
                                     thumbnail: "robot-thumb",
                                     jointFrames: HistoryRawData.hist)
}

extension simd_float4x4: Codable {
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let columns = try values.decode([SIMD4<Float>].self, forKey: .columns)
        self.init(columns)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode([columns.0, columns.1, columns.2, columns.3], forKey: .columns)
    }
    
    private enum CodingKeys: String, CodingKey {
        case columns
    }
}

extension simd_quatf: Codable {
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let vector = try values.decode(simd_float4.self, forKey: .vector)
        self.init(vector: vector)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(vector, forKey: .vector)
    }
    
    private enum CodingKeys: String, CodingKey {
        case vector
    }
}
