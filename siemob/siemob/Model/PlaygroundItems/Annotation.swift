//
//  Annotation.swift
//  siemob
//
//  Created by Yifeng Dong on 22.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import RealityKit
import UIKit

class Annotation: PlaygroundItem {
    static let newAnnotation = Annotation(name: "New Annotation", thumbnail: "annotation_thmb")
}
