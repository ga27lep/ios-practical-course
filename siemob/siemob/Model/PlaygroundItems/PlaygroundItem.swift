//
//  Item.swift
//  siemob
//
//  Created by Yifeng Dong on 21.11.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import ModelIO
import Poly
import RealityKit

public class PlaygroundItem: Identifiable, Codable {
    private static let materialFileExtensions = ["png", "jpg", "jpeg"]
    
    public let id: String
    let name: String
    let thumbnail: String?
    let resource: String?
    
    var resourceURL: URL? {
        Bundle.main.url(forResource: resource, withExtension: "usdz")
    }
    
    var scale: Float
    
    var available: Bool {
        resourceURL != nil
    }
    
    func load() -> Entity? {
        guard let resourceURL = resourceURL else {
            return nil
        }
        
        let loadedEntity: Entity
        
        do {
            try loadedEntity = Entity.load(contentsOf: resourceURL)
        } catch {
            print("Could not load entity asset")
            return nil
        }
        
        loadedEntity.scale = [scale, scale, scale]
        
        return loadedEntity
    }
    
    init(name: String, thumbnail: String? = "", resource: String? = "", scale: Float = 1.00, thumbnailURL: String? = "", id: String? = nil) {
        self.name = name
        self.thumbnail = thumbnail
        self.resource = resource
        self.scale = scale
        self.id = id ?? UUID().description
        //self.thumbnailURL = thumbnailURL
    }
}

extension PlaygroundItem: Equatable {
    public static func == (lhs: PlaygroundItem, rhs: PlaygroundItem) -> Bool {
        return lhs.id == rhs.id
    }
}
