//
//  PolyConttroller.swift
//  siemob
//
//  Created by Mehmet Baris Yaman on 19.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

//import Foundation
//import Combine
//import Poly
//
//public class PolySearch {
//    var polySession = Poly.shared
//    
//    public func getSearchResults(for keywords: [String]) -> AnyPublisher<[PlaygroundItem], Never> {
//        return listAssetsUsingKeywords(keywords: keywords).map {
//            $0.map { asset in
//                PlaygroundItem(polyAsset: asset)
//            }
//        }
//        .catch { _ in
//            return Just([])
//        }
//        .eraseToAnyPublisher()
//    }
//    
//    public func listAssetsUsingKeywords(keywords: [String]) -> Future<[PolyAssetModel], Error> {
//        return Future<[PolyAssetModel], Error> { promise in
//            self.polySession.list(assetsWithKeywords: keywords) { newAssets, total, next, error in
//                if let error = error {
//                    promise(.failure(error))
//                }
//                
//                if let newAssets = newAssets {
//                    promise(.success(newAssets))
//                }
//            }
//        }
//    }
//}
