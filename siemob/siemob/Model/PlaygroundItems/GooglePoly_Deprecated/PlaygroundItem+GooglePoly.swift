//
//  PlaygroundItem+GooglePoly.swift
//  siemob
//
//  Created by Yifeng Dong on 23.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation

/// Google Poly:

//extension PlaygroundItem {
//init(polyAsset: PolyAssetModel, scale: Float = 1.00) {
//    self.name = polyAsset.displayName ?? "Untitled"
//    self.thumbnailURL = polyAsset.thumbnail?.url
//    self.id = String(polyAsset.identifier.dropFirst("assets/".count))
//
//    self.scale = scale
//    self.thumbnail = nil
//}

//        func fill() {
//            guard !available else {
//                return
//            }
//
//            Poly.shared.download(assetWithIdentifier: self.id, progressHandler: nil) { rootFileUrl, resourceFiles, error in
//                guard let rootFileUrl = rootFileUrl else { return }
//
//                // URL of downloaded .obj file
//                self.resourceURL = rootFileUrl
//
//                if rootFileUrl.pathExtension == "obj" {
//                    guard let resourceFiles = resourceFiles else { return }
//
//                    // Check for .jpg, .png, .jpeg files: Assume these are the base color textures
//                    for resource in resourceFiles {
//                        if Self.materialFileExtensions.contains(resource.pathExtension) {
//                            self.baseColorURL = resource
//                        }
//                    }
//
//                    let objAsset = MDLAsset(url: rootFileUrl)
//
//                    guard let object = objAsset.object(at: 0) as? MDLMesh else {
//                        fatalError("Failed to get mesh from asset.")
//                    }
//
//                    if let baseColorURL = self.baseColorURL {
//                        let scatteringFunction = MDLScatteringFunction()
//                        let material = MDLMaterial(name: "baseMaterial", scatteringFunction: scatteringFunction)
//
//                        material.setTextureProperties(textures: [.baseColor: baseColorURL])
//
//                        // Apply the texture to every submesh of the asset
//                        for submesh in object.submeshes ?? [] {
//                            if let submesh = submesh as? MDLSubmesh { submesh.material = material }}
//                    }
//
//                    let destinationFileUrl = rootFileUrl.appendingPathExtension("")
//
//                    // Export to USD
//                    if MDLAsset.canExportFileExtension("abc") {
//                        do {
//                            try objAsset.export(to: destinationFileUrl)
//                        } catch let error {
//                            print(error)
//                        }
//
//                        self.resourceURL = destinationFileUrl; print(destinationFileUrl.absoluteString)
//                    } else {
//                        // print("Exporting to \("abc") not supported")
//                    }
//                    /*objAsset.exportToUSDZ(destinationFileUrl: destinationFileUrl) { completed, error in
//                     if !completed {
//                     print(error.debugDescription)
//                     return
//                     }
//
//                     self.resourceURL = destinationFileUrl
//                     print(destinationFileUrl)
//                     }*/
//                }
//            }
//        }
//}
//
///**
// Credits: http://iosdeveloperzone.com/2016/05/10/getting-started-with-modelio/
// */
//extension MDLMaterial {
//    func setTextureProperties(textures: [MDLMaterialSemantic : URL]) -> Void {
//        for (key,value) in textures {
//            let property = MDLMaterialProperty(name: value.lastPathComponent, semantic: key, url: value)
//            self.setProperty(property)
//        }
//    }
//}
//
//private let usdcFileExtension = "usda"
//private let usdzFileExtension = "usdz"
//
//extension MDLAsset {
//
//    /// export/convert an OBJ file to USDZ format on device
//    ///
//    /// - Parameters:
//    ///   - destinationUrl: USDZ destination file URL, ex: "path/example.usdz"
//    ///   - completionHandler: completion handler for the operation, (completed, error)
//    public func exportToUSDZ(destinationFileUrl: URL, completionHandler: ((Bool, Error?) -> Void)? = nil) {
//        // check if destinationUrl is valid
//        guard FileManager.default.fileExists(atPath: destinationFileUrl.path) == false else {
//            DispatchQueue.main.async {
//                completionHandler?(false, nil)
//            }
//            return
//        }
//
//        // check if destination is ".usdz"
//        guard destinationFileUrl.pathExtension == usdcFileExtension else {
//            DispatchQueue.main.async {
//                completionHandler?(false, nil)
//            }
//            return
//        }
//
//        // export the .obj asset and create a temp .usdc file
//        let usdcDestinationFileUrl = destinationFileUrl.deletingPathExtension().appendingPathExtension(usdcFileExtension)
//        if MDLAsset.canExportFileExtension(usdcFileExtension) {
//            do {
//                try self.export(to: usdcDestinationFileUrl)
//            } catch let error {
//                DispatchQueue.main.async {
//                    completionHandler?(false, error)
//                }
//            }
//        } else {
//            DispatchQueue.main.async {
//                completionHandler?(false, nil)
//            }
//        }
//
//
//        /*// rename the .usdc file to .usdz
//         // https://www.scandy.co/blog/how-to-export-simple-3d-objects-as-usdz-on-ios
//         do {
//         try FileManager.default.moveItem(at: usdcDestinationFileUrl, to: destinationFileUrl)
//         } catch let error {
//         DispatchQueue.main.async {
//         completionHandler?(false, error)
//         }
//         }*/
//
//        // success!
//        DispatchQueue.main.async {
//            completionHandler?(true, nil)
//        }
//    }
//}
