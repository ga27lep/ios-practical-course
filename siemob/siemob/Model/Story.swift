//
//  Story.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import CoreData

@objc(Story)
public class Story: NSManagedObject {
    
       public var textValue: String {
           return self.text ?? ""
       }
       
       public var storyValue: String {
             return self.story ?? ""
         }

}

// MARK: - Extension: Storable
extension Story: Storable {
    static var entityName: String = "Story"
   
}

// MARK: - Extension: Identifiable
extension Story: Identifiable { }


// MARK: - Extension: Comparable
extension Story: Comparable {
    public static func < (lhs: Story, rhs: Story) -> Bool {
        guard let lhsOrder = lhs.order, let rhsOrder = rhs.order else {
            return true
        }
        guard let lhsNum = Int(lhsOrder), let rhsNum = Int(rhsOrder) else {
            return true
        }
        return lhsNum > rhsNum
    }
}


extension Story {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Story> {
        return NSFetchRequest<Story>(entityName: "Story")
    }
    
    @NSManaged public var id: UUID
    @NSManaged public var order: String?
    @NSManaged public var text: String?
    @NSManaged public var story: String?

}
