//
//  Storable.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import CoreData

// MARK: - Storable
/// An object that can be saved by CoreData
protocol Storable {
    static var entityName: String { get }
}

// MARK: Extension: Storable: Load & Save
extension Storable {

    /**
     Fetch an array of Storables from CoreData
     - returns: An array of deserialised objects
     */
    static func fetchEntities() throws -> [Self] {
        let context = Model.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
        
        let fetchedEntities = try context.fetch(fetchRequest)
        
        guard let entities = fetchedEntities as? [Self] else {
            print("Failed to cast entities")
            return []
        }

        return entities
    }
}
