//
//  KeychainStorage.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//


import Foundation

struct KeychainStorage: TokenStorage {
    
    let secureStore: SecureStore
    
    init(secureStore: SecureStore) {
        self.secureStore = secureStore
    }
    
    
    func storeToken(_ token: String) {
        do {
          try secureStore.setValue(token)
        } catch let error {
          print("Saving token failed with \(error.localizedDescription).")
        }
    }
    
    func loadToken() -> String {
        do {
            guard let token = try secureStore.getValue() else {
                print("Tried to read token, which was nil")
                return ""
            }
            return token
        } catch let error {
          print("Reading token failed with \(error.localizedDescription).")
        }
        return ""
    }
}
