//
//  UserDefaultsStorage.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation

struct UserDefaultsStorage: TokenStorage {
    
    let tokenKey = "token"
    
    func storeToken(_ token: String) {
        UserDefaults.standard.set(token, forKey: tokenKey)
    }
    
    func loadToken() -> String {
        return UserDefaults.standard.string(forKey: tokenKey) ?? ""
    }
}
