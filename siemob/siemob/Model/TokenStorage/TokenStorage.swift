//
//  TokenStorage.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation

enum TokenStoringMethod {
    case userDefaults
    case keychain
}

protocol TokenStorage {
    func storeToken(_ token: String)
    func loadToken() -> String
}
