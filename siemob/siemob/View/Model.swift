//
//  Model.swift
//  siemob
//
//  Created by Yifeng Dong on 21.11.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import Combine
import CoreData

public class Model {
    @Published public private(set) var library: ItemLibrary
    @Published public var projects: [Project]
    @Published public var currentProjectID: String
    @Published public var stories: [Story]
    @Published public var collaborationOpened: Bool = false
    @Published public var authenticated: Bool = false
    @Published public var invalidLoginText: String = ""
    var cancellable: AnyCancellable?
    
    
    // Token Storage
    internal let tokenStorage: TokenStorage = {
        switch Persistence.tokenStoringMethod {
        case .userDefaults:
            return UserDefaultsStorage()
        case .keychain:
            return KeychainStorage(secureStore:
                SecureStore(secureStoreQueryable:
                    GenericPasswordQueryable(service: "someService")
                )
            )
        }
    }()
    
    
    // MARK: - Initializers
    public init(library: ItemLibrary? = nil, projects: [Project]? = nil, stories: [Story]? = nil) {
        if Persistence.isEnabled {
            self.stories = stories ?? Model.loadStories()
            self.projects = projects ?? Model.loadProjects()
        } else {
            self.stories = stories ?? []
            self.projects = projects ?? []
        }
        self.library = library ?? ItemLibrary.mockLibrary // TODO: For now.
        self.currentProjectID = ""
    }
    
    private static func loadProjects() -> [Project] {
        do {
            let projects = try Project.fetchEntities()

            return projects
        } catch {
            print("Failed to fetch entities of '\(Project.entityName)': \(error)")

            return []
        }
    }
    
    private static func loadStories() -> [Story] {
        do {
            let stories = try Story.fetchEntities()

            return stories
        } catch {
            print("Failed to fetch entities of '\(Story.entityName)': \(error)")

            return []
        }
    }
    
    
    
    public func project(_ id: Project.ID?) -> Project? {
        projects.first(where: { $0.id == id })
    }
    
    public func saveProject(id: Project.ID?, name: String) {
        guard let id = id else {
            return
        }
        // Create a new managed story object
        let managedContext = Model.persistentContainer.viewContext
        var project: Project

        // Create account object

        if Persistence.isEnabled {
            project = createManagedProject(id: id, name: name)
        } else {
            project = Project(context: managedContext)

            project.id = UUID()
            project.name = name
        }

        delete(project: project.id)
        projects.append(project)
        projects.sort()

        return
    }
    
    private func createManagedProject(id: Project.ID ,name: String) -> Project {
        let managedContext = Model.persistentContainer.viewContext
        let project = Project(context: managedContext)

        project.id = id
        project.name = name

        Model.saveContext()

        return project
    }
    
    public func delete(project id: Project.ID) {
        projects.removeAll(where: { $0.id == id })
    }

    
    
    public func story(_ id: Story.ID?) -> Story? {
        stories.first(where: { $0.id == id })
    }
    
    public func saveStory(id: Story.ID?, text: String, filledStory: String, order: Int) {
        guard let id = id, let story = self.story(id) else {

            // Create a new managed story object
            let managedContext = Model.persistentContainer.viewContext
            var story: Story

            // Create account object

            if Persistence.isEnabled {
                story = createManagedStory(text: text, filledStory: filledStory, order: order)
            } else {
                story = Story(context: managedContext)

                story.id = UUID()
                story.text = text
                story.order = String(order)
            }

            story.story = filledStory

            delete(story: story.id)
            stories.append(story)
            stories.sort()

            return
        }

        // Edit existing account object
        
        story.text = text
        story.story = filledStory
        story.order = String(order)

        Model.saveContext()

        stories.sort()
    }
    
    private func createManagedStory(text: String, filledStory: String, order: Int) -> Story {
        let managedContext = Model.persistentContainer.viewContext
        let story = Story(context: managedContext)

        story.id = UUID()
        story.text = text
        story.story = filledStory
        story.order = String(order)

        Model.saveContext()

        return story
    }

    public func delete(story id: Story.ID) {
        stories.removeAll(where: { $0.id == id })
    }

}

// MARK: - Extension: ObservableObject
extension Model: ObservableObject { }

// MARK: - Extension: Mock Model
extension Model {
    public static var mock = Model(library: ItemLibrary.mockLibrary)
}

// MARK: Extension: Model: Core Data stack
extension Model {
    // MARK: - Core Data stack
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "siemob")
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
