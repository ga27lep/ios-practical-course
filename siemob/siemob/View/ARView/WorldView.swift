//
//  ARController.swift
//  siemob
//
//  Created by Yifeng Dong on 28.11.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import RealityKit
import ARKit
import AVFoundation
import MultipeerConnectivity

#if targetEnvironment(simulator)
#else

/// This view contains all of the (low-level) RealityKit functionality.
/// This view is of type WorldView, which is our custom subclass of Apple's default RealityKit class ARView
class WorldView: ARView {
    // WorldView needs to get the state information it needs to perform low-level user interaction.
    var objectToAdd: PlaygroundItem?
    var arGlobalState: ARGlobalState!
    var simulationButton: SimulationState?
    var drawing: DrawingState?
    
   // let coachingOverlay = ARCoachingOverlayView()
    
    // Weak reference to its parent view controller
    weak var parentViewController: ARViewController!
    
    var animations = Animation().animations

    var audioPlayer: AVAudioPlayer?
    private var anchorList: [(object: (anchor: AnchorEntity, position: SIMD3<Float>) , index: Int)] = []
    public var musicBox: (object: (anchor: AnchorEntity, position: SIMD3<Float>) , index: Int)?
    private var avatarsListNew: [Avatar] = []
    private var index: Int = 0
    private var avatarIndex: Int = 0
    private var simulationAudio: Bool = false
    private var generateAnchors: Bool = false
    var radioObjectIndex = -1
    var gymnasticIndex = -1
    private var parentExist = false
    private var parentAnchor = AnchorEntity()
    let pearl = (PlaygroundObject(name: "pearl",
                               thumbnail: "pearl-thmb",
                               resource: "FemaleLayingPose3",scale: 0.001))
   public var playOnce: Int = 1
    
    @objc func fire() {
//        if parentExist == false {
//            avatarsListNew.forEach( {if $0.animationDescription.nr == 3 {
//                let obj = $0.objectLastMovedTo!
//                self.loadParent(anchorEntity: $0.anchorEntity, obj: obj)
//            }})
//            parentExist = true
//        } else {
//            parentExist = false
//            self.scene.removeAnchor(self.parentAnchor)
//        }
    }
    private func generateRandomAnchors() {
        print("anchorList.count", anchorList.count)
        if anchorList.count < 2 {
            return
        }
        var anchor1: (object: (anchor: AnchorEntity, position: SIMD3<Float>), index: Int) = anchorList.first!
        var count = anchorList.count - 1
        var index = 1
        while count >= 1 {
            let anchor2 = anchorList.first(where: { index ==  $0.index })!
            let randomPosition = getRandomPosition(position1: anchor1.object.position, position2: anchor2.object.position)
            let newAnchor = AnchorEntity(world: randomPosition)
         //   let modelEntity = ModelEntity(mesh: MeshResource.generateBox(size: 0.1))
         //   newAnchor.addChild(modelEntity)
       //     self.loadParent(position: randomPosition)
            self.scene.addAnchor(newAnchor)
            anchor1 = anchor2
            anchorList.append((object: (anchor: newAnchor, position: randomPosition), index: self.index))
            self.index += 1
            count -= 1
            index += 1
        }
    }
    
    private func getRandomPosition(position1: SIMD3<Float>, position2: SIMD3<Float>) -> SIMD3<Float> {
       // let randomScale = Float.random(in: 2 ..< 4)
        let middlePoint = (position1 + position2) / 2
        //middlePoint.z *= randomScale
        return middlePoint
    }
    
//    private func loadParent(position: SIMD3<Float>) {
//        //var pos = anchorEntity.position(relativeTo: obj)
////        pos.x -= 0.15
////        pos.z -= 0.15
//        self.parentAnchor = AnchorEntity(world: position)
//        let parentEntity: Entity
//        let parentURL = Bundle.main.url(forResource: "Yelling", withExtension: "usdz") ?? URL(fileURLWithPath: "")
//        do {
//            try parentEntity = Entity.load(contentsOf: parentURL)
//        } catch {
//            print("Could not load entity asset"); return
//        }
//        parentEntity.scale = [pearl.scale, pearl.scale, pearl.scale]
//
//        let parentOfParentEntity = ModelEntity()
//        parentOfParentEntity.addChild(parentEntity)
//        self.parentAnchor.addChild(parentEntity)
//        self.scene.anchors.append(self.parentAnchor)
//        self.parentList.append(parentAnchor)
//        parentEntity.availableAnimations.forEach {
//            parentEntity.playAnimation($0.repeat())
//        }
//    }
    
    /// Tells the view that one or more new touches occurred.
    /// This is where we handle adding Entities, as well as Avatars in simulation mode.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // We only handle one touch at a time.
        guard let first = touches.first else { return }; if arGlobalState.simulation {arGlobalState.pressedSimulationButton = false}
        
        // Have we selected an object to add in the toolbar?
        if let objectToAdd = arGlobalState.selectedItemToAdd {


            guard objectToAdd.available else { return }

            // If the object isn't downloaded to disk, download it.

            //guard objectToAdd.resourceURL != nil else { objectToAdd.fill(); return
            if objectToAdd.name == "MusicBox" {
                 radioObjectIndex = self.index
            }
            
            if objectToAdd.name == "Gymnastic" {
                gymnasticIndex = self.index
            }
            // Get the point in the 3D world where our object should be (hit-test from the touch to any horizontal surface)
            guard let hitTestResult = hitTest(first.location(in: self), types: [.existingPlaneUsingGeometry, .estimatedHorizontalPlane]).first else { return }; print("hitTestResult: ", hitTestResult)

            // Get the point in the 3D world where our object should be (raycast from the touch to any horizontal surface)
            //guard let raycastResult = raycast(from: first.location(in: self), allowing: .estimatedPlane, alignment: .horizontal).first else {
            //    return
            //}
            
            // Deselect the item in the toolbar
            self.arGlobalState?.selectedItemToAdd = nil
            
            if objectToAdd is Annotation {
                addBillboard(touch: first)
                return
            }
            
            let arAnchor = hitTestResult.anchor ?? ARAnchor(transform: hitTestResult.worldTransform)
            self.session.add(anchor: arAnchor)
            let anchorEntity = AnchorEntity(anchor: arAnchor)
            
            let objectPosition = SIMD3<Float>(hitTestResult.worldTransform.columns.3.x, hitTestResult.worldTransform.columns.3.y,hitTestResult.worldTransform.columns.3.z)
            
            let playgroundEntity: PlaygroundEntity
            
            if let objectToAdd = objectToAdd as? RecordedAction {
                // Custom actions for recorded actions:
                playgroundEntity = RecordedActionEntity(fromAction: objectToAdd)
                guard let playgroundEntity = playgroundEntity as? RecordedActionEntity else { return }
                parentViewController.recordedActionEntities.append(playgroundEntity)
            } else {
                // Our own custom entity class which is essentially like a ModelEntity without Physics
                playgroundEntity = PlaygroundEntity(item: objectToAdd)
            }
            
            playgroundEntity.setupEntity(inView: self)
            
            anchorEntity.addChild(playgroundEntity); self.scene.anchors.append(anchorEntity);
            anchorEntity.synchronization?.ownershipTransferMode = .autoAccept
            
           // if radioObjectIndex == -1 { radioObjectIndex = self.index }
            if objectToAdd.name == "MusicBox" {
                musicBox = (object: (anchor: anchorEntity, position: objectPosition), index: self.index)
            }
            anchorList.append((object: (anchor: anchorEntity, position: objectPosition), index: self.index)); self.index += 1
            
            // Add playgroundEntity as a child of anchorEntity
            // Our resulting hierarchy is now anchorEntity -> playgroundEntity -> loadedEntity
            let sound = Bundle.main.url(forResource: "place_something", withExtension: "wav") ?? URL(fileURLWithPath: "")
            do { try audioPlayer = AVAudioPlayer(contentsOf: sound) } catch { print("Could not load entity asset"); return
            }; audioPlayer?.play()
         
        } else {
          //  let timer = Timer(timeInterval: 3.0, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
            if simulationAudio == false {
                let sound = Bundle.main.url(forResource: "playground_ambience", withExtension: "mp3") ?? URL(fileURLWithPath: "")
                do {
                    try audioPlayer = AVAudioPlayer(contentsOf: sound)
                } catch {
                    print("Could not load entity asset")
                    return
                }
                audioPlayer?.numberOfLoops = 10
                audioPlayer?.play()
                simulationAudio = true
            }
            if arGlobalState.simulation {
            if generateAnchors == false { generateRandomAnchors(); generateAnchors = true }
             //  RunLoop.current.add(timer, forMode: .common)
                guard let hitTestResult = hitTest(first.location(in: self), types: [.existingPlaneUsingGeometry, .estimatedHorizontalPlane])
                    .first else { return }
                self.arGlobalState?.selectedItemToAdd = nil
                let anchorEntity = AnchorEntity(world: hitTestResult.worldTransform)
                if let musicBox = self.musicBox {
                    let avatar = Avatar(anchor: anchorEntity, objects: anchorList, radioObjectIndex: radioObjectIndex, simulationStarted: true, gymnasticIndex: gymnasticIndex, playOnce: playOnce, musicBox: musicBox)
                    playOnce += 1
                    self.scene.anchors.append(anchorEntity)
                    avatarsListNew.append(avatar)
                }
                else {
                    let avatar = Avatar(anchor: anchorEntity, objects: anchorList, radioObjectIndex: radioObjectIndex, simulationStarted: true, gymnasticIndex: gymnasticIndex, playOnce: playOnce)
                    playOnce += 1
                    self.scene.anchors.append(anchorEntity)
                    avatarsListNew.append(avatar)
                }
               
            }
            
            if !arGlobalState.simulation {
              //  timer.invalidate()
                if simulationAudio { audioPlayer?.stop(); simulationAudio = false }; let anchors = self.scene.anchors
                anchors.forEach({ $0.stopAllAnimations(); if $0.findEntity(named: "Hips") != nil { self.scene.removeAnchor($0.anchor!) } })
                avatarsListNew.forEach({ $0.simulationStarted = false })
                avatarsListNew = []
            }
        }
        return
    }
    
    func addBillboard(touch: UITouch) {
        // Get the user's tap screen location.
        let touchLocation = touch.location(in: self)
        
        // Cast a ray to check for its intersection with any planes.
        guard let hitTestResult = hitTest(touchLocation, types: [.existingPlaneUsingGeometry, .estimatedHorizontalPlane]).first else { return }
        
        // Create a new sticky note positioned at the hit test result's world position.
        let frame = CGRect(origin: touchLocation, size: CGSize(width: 500, height: 500))

        let billboard = BillboardEntity(frame: frame, worldTransform: hitTestResult.worldTransform)
        
        // Center the sticky note's view on the tap's screen location.
        billboard.setPositionCenter(touchLocation)

        // Add the sticky note to the scene's entity hierarchy.
        self.scene.addAnchor(billboard)

        // Add the sticky note's view to the view hierarchy.
        guard let stickyView = billboard.view else { return }
        self.insertSubview(stickyView, belowSubview: parentViewController.trashZone)

        // Enable gestures on the sticky note.
        parentViewController.billboardGestureSetup(billboard)
        
        // Save a reference to the sticky note.
        parentViewController.billboards.append(billboard)
        
        
        // Volunteer to handle text view callbacks.
        stickyView.textView.delegate = parentViewController
    }
}

//extension WorldView: ARCoachingOverlayViewDelegate {
  // func addCoaching() {
    //  self.coachingOverlay.delegate = self
     //self.coachingOverlay.session = self.session
     //self.coachingOverlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      // MARK: CoachingGoal
    //  self.coachingOverlay.goal = .horizontalPlane
      
     // self.addSubview(self.coachingOverlay)
   // }
   // public func coachingOverlayViewDidDeactivate(_ coachingOverlayView: ARCoachingOverlayView) {
    //  coachingOverlayView.activatesAutomatically = false
   // }

//}
#endif
