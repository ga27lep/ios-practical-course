//
//  CameraView.swift
//  SiemobBeta
//
//  Created by Jass on 11/4/19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

struct MockARView: View {
    @EnvironmentObject var model: Model
    
    var body: some View {
      
            Color(.black) 
            
        
    }
}

struct CameraView_Previews: PreviewProvider {
    static var previews: some View {
        MockARView().previewLayout(.fixed(width: 1366, height: 1024)).environmentObject(Model.mock)
    }
}
