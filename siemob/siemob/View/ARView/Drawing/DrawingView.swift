//
//  DrawingView.swift
//  siemob
//
//  Created by Jessica on 1/23/20.
//  Copyright © 2020 TUM. All rights reserved.
//

import SwiftUI

struct DrawingView: View {
     @ObservedObject var drawing: DrawingState
   
    var body: some View {
        drawingView
    }
    var drawingView: some View {
        VStack {
            if self.drawing.tappedSetting {
                HStack {
                    colors
                    lineWidth
                }
            }
            HStack {
                if !drawing.drawingMode {
                    notDrawingButton
                }
            HStack {
                if drawing.drawingMode {
                    drawingButton
                }
                HStack {
                    if drawing.drawingMode {
                        drawingMode
                        Spacer()
                        deleteCompletely
                        Spacer()
                        drawFence
                        eraseDrawing
                        }
                    
                }
                }.background(Color(.black).cornerRadius(30)).padding(.bottom, -10).opacity(0.7)
                }
        }
    }
    var colors: some View {
        HStack {
            green
            brown
            blue
            yellow
            red
            black
        }
    }
    var lineWidth: some View {
        HStack {
            incrementLineWidth
            decrementLineWidth
        }
    }
    
    var incrementLineWidth: some View {
        Button(action: {
            if self.drawing.tappedDraw {
                self.drawing.lineWidthDrawing =  self.drawing.lineWidthDrawing + 0.02
            }
            if self.drawing.drawFence {
                self.drawing.lineWidthFence = self.drawing.lineWidthFence + 0.02
            }
        } , label: {Image(systemName: "plus.circle")
            .resizable()
            .frame(width: 30, height: 30)})
            .padding()
            .foregroundColor(.white)
    }
    var decrementLineWidth: some View {
        Button(action: {
            if self.drawing.tappedDraw {
                self.drawing.lineWidthDrawing = self.drawing.lineWidthDrawing - 0.02
            }
            if self.drawing.drawFence {
                self.drawing.lineWidthFence = self.drawing.lineWidthFence - 0.02
            }
            
        } , label: {Image(systemName: "minus.circle")
            .resizable()
            .frame(width: 30, height: 30)})
            .padding()
            .foregroundColor(.white)
    }
    
    var deleteCompletely: some View {
        Button(action: {
            self.drawing.drawingMode = false
            self.drawing.tappedSetting = false
        } , label: {Image(systemName: "return")
            .resizable()
            .frame(width: 50, height: 50)})
            .padding()
            .foregroundColor(.white)
    }
    var drawingMode: some View {
        Button(action: { self.drawing.tappedSetting.toggle() }, label: { Image(systemName: "slider.horizontal.3").resizable()
            .frame(width: 70, height: 70)})
            .foregroundColor(.white)
    }
    
    
    var drawingButton: some View {
        Button(action: {
            if !self.drawing.tappedDraw {
                self.drawing.drawCounter + 1
                print(self.drawing.drawCounter)
            }
            self.drawing.tappedDraw.toggle()
            self.drawing.tappedSetting = false
            self.drawing.isErase = false
        }) {
            if self.drawing.tappedDraw {
                Image(systemName: "nosign")
                    .resizable()
                    .frame(width: 70, height: 70)
                    .padding()
                    .foregroundColor(.red)
            }
            else {
                Image(systemName: "scribble")
                    .resizable()
                    .frame(width: 70, height: 70)
                    .foregroundColor(.white)
                    .padding(.leading).padding()
            }
        }
        .padding(.leading)
        .padding(.bottom)
    }
    var eraseDrawing: some View {
        Button(action: {self.drawing.isErase.toggle()
            self.drawing.tappedDraw = false
            self.drawing.drawFence = false
            if self.drawing.drawCounter > 0 {
                self.drawing.drawCounter - 1
            }
        }) {
            Image(systemName: "trash")
                .resizable()
                .frame(width: 70, height: 70)
                .padding()
                .foregroundColor(.red)
        }
    }
    
    var drawFence: some View {
        Button(action: {
            self.drawing.drawFence.toggle()
            self.drawing.isErase = false
        }) {
            if self.drawing.drawFence {
                Image(systemName: "nosign")
                    .resizable()
                    .frame(width: 70, height: 70)
                    .padding()
                    .foregroundColor(.red)
            } else {
                Image(systemName: "text.justify")
                    .resizable()
                    .frame(width: 70, height: 70)
                    .padding()
                    .foregroundColor(Color(.brown))
            }
        }
    }
    
    var notDrawingButton: some View {
        Button(action: {self.drawing.drawingMode = true; }, label: {
            Image(systemName: "paintbrush")
                .resizable()
                .frame(width: 60, height: 60)
                .padding()
                .foregroundColor(.white)
        })
    }
    
    
    var blue: some View {
        Button(action: {
            if self.drawing.tappedDraw {
                self.drawing.colorDraw = .blue
            }
            if self.drawing.drawFence {
                self.drawing.colorFence = .blue
            }
        })
            
        {
            Color(.blue)
                .frame(width: 50, height: 50)
                .cornerRadius(50)
        }
    }
    var brown: some View {
        Button(action: { if self.drawing.tappedDraw {
            self.drawing.colorDraw = .brown
            }
            if self.drawing.drawFence {
                self.drawing.colorFence = .brown
            }}) {
                Color(.brown)
                    .frame(width: 50, height: 50)
                    .cornerRadius(50)
        }
    }
    var green: some View {
        Button(action: {if self.drawing.tappedDraw {
            self.drawing.colorDraw = .green
            }
            if self.drawing.drawFence {
                self.drawing.colorFence = .green
            }}) {
                Color(.green)
                    .frame(width: 50, height: 50)
                    .cornerRadius(50)
        }
    }
    var yellow: some View {
        Button(action: {if self.drawing.tappedDraw {
            self.drawing.colorDraw = .yellow
            }
            if self.drawing.drawFence {
                self.drawing.colorFence = .yellow
            }}) {
                Color(.yellow)
                    .frame(width: 50, height: 50)
                    .cornerRadius(50)
        }
    }
    var red: some View {
        Button(action: {if self.drawing.tappedDraw {
            self.drawing.colorDraw = .red
            }
            if self.drawing.drawFence {
                self.drawing.colorFence = .red
            }}) {
                Color(.red)
                    .frame(width: 50, height: 50)
                    .cornerRadius(50)
        }
    }
    var black: some View {
        Button(action: {
            if self.drawing.tappedDraw {
                self.drawing.colorDraw = .black
            }
            if self.drawing.drawFence {
                self.drawing.colorFence = .black
            }}) {
                Color(.black)
                    .frame(width: 50, height: 50)
                    .cornerRadius(50)
        }
    }
    
}

//struct DrawingView_Previews: PreviewProvider {
//   
////    static var previews: some View {
////        DrawingView(drawing: drawing)
////    }
//}
