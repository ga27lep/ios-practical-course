//
//  DrawingState.swift
//  siemob
//
//  Created by Jessica on 1/9/20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import SwiftUI

class DrawingState: ObservableObject {
   
    
    @Published var lineWidthDrawing: Float = 0.06
    @Published var lineWidthFence: Float = 0.06
    
    @Published var isErase: Bool = false
    
    @Published var drawCounter: Int = -1
       //to show sliders
    @Published var tappedSetting: Bool = false
       //click to start drawing
    @Published var tappedDraw: Bool = false
       //general entering the mode
    @Published var drawingMode: Bool = false
    
    //still connect it to button in MainView!!!!
    @Published var drawFence: Bool = false
    
    
    @Published var colorDraw: UIColor = .green
    @Published var colorFence: UIColor = .brown
}
