////
////  CameraAR.swift
////  siemob
////
////  Created by Jass on 11/8/19.
////  Copyright © 2019 TUM. All rights reserved.
////
//

#if targetEnvironment(simulator)
#else
import SwiftUI
import RealityKit
import ARKit

/// Main SwiftUI view that contains the camera view
struct ARCameraView: View {
    // EnvironmentObjects and ObservedObjects can be accessed from this view like a normal SwiftUI view.
    @EnvironmentObject var arGlobalState: ARGlobalState
    @ObservedObject var drawing: DrawingState
   
    
    var body: some View {
        // Return the wrapped UIKit ViewController
        return ARViewContainer(arGlobalState: arGlobalState,
                               selectedItemToAdd: $arGlobalState.selectedItemToAdd, drawing: drawing)
    }
}

/// Container for the UIKit ViewController containing the AR view
struct ARViewContainer: UIViewControllerRepresentable {
    @ObservedObject var arGlobalState: ARGlobalState
    
    // The redundant @Binding is there because of a Swift bug in @ObservedObject that doesn't cause
    // the view to refresh properly if arGlobalState is changed.
    @Binding var selectedItemToAdd: PlaygroundItem?
    @EnvironmentObject var model: Model
    @EnvironmentObject var multipeerHelper: MultipeerHelper

    @ObservedObject var drawing: DrawingState
    

    // Creates a view controller instance to present.

    func makeUIViewController(context: Context) -> ARViewController {
        let arViewController = ARViewController()
        
        // State objects relevant to the AR view have to be passed to the ViewController
        arViewController.arGlobalState = arGlobalState
        arViewController.drawing = drawing
        arViewController.model = model
        arViewController.multipeerHelper = multipeerHelper
        
        return arViewController
    }

    
    // We might not have to do this at all.
    func updateUIViewController(_ uiView: ARViewController, context: Context) {
        uiView.objectToAdd = arGlobalState.selectedItemToAdd
        uiView.drawing = drawing
    }
}
#endif
