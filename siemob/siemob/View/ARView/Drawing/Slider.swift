//
//  Slider1.swift
//  siemob
//
//  Created by Jessica on 12/29/19.
//  Copyright © 2019 TUM. All rights reserved.
//
//
import SwiftUI
import RGSColorSlider

struct Sliders: View {
    @Binding var colorV: UIColor
    @State var value: CGFloat
    var minimumValue: CGFloat = 1.0
    var maximumvalue: CGFloat = 10.0
       
    var body: some View {
        VStack {
            HStack {
                Text("Line Width = \( value) cm")
                    .padding(.leading)
                Spacer()
            }
            Slider(value: $value, in: minimumValue...maximumvalue)
                .padding(.bottom)
      ColorSlider(colorV: $colorV)
          //  Text(" \(colorV) ").background(Color(colorV))
        }
        
    }
}

struct Sliders_Previews: PreviewProvider {
    static var previews: some View {
        Sliders(colorV: .constant(.red), value: 2)
    }
}
