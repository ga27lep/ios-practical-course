//
//  SLider.swift
//  siemob
//
//  Created by Jessica on 12/29/19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI
import RGSColorSlider

struct ColorSlider: UIViewRepresentable {
   @Binding var colorV: UIColor
  // @Binding var value: Double
    
    class Coordinator: NSObject {
        var parent: ColorSlider
        init(parent: ColorSlider) {
            self.parent = parent
        }
    
        @objc func valueChanged(_ sender: RGSColorSlider) {
           // self.parent.value = Double(sender.value)
            self.parent.colorV = sender.color
        }
    }
    
    
    func makeCoordinator() -> ColorSlider.Coordinator {
        Coordinator(parent: self)
    }
    
    func makeUIView(context: Context) -> RGSColorSlider {
        let slider = RGSColorSlider(frame: .zero)
       // slider.value = Float(value)
        slider.addTarget(context.coordinator, action: #selector(Coordinator.valueChanged(_:)), for: .valueChanged)
        return slider
    }
    
    func updateUIView(_ uiView: RGSColorSlider, context: Context) {
       // uiView.value = Float(value)
        uiView.color = colorV
    }
}


struct ColorSlider_Previews: PreviewProvider {
   
    static var previews: some View {
        ColorSlider(colorV: .constant(.red))
        }
        
    }


