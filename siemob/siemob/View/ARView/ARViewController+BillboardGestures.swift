//
//  ARViewController+BillboardGestures.swift
//  siemob
//
//  Created by Yifeng Dong on 18.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import RealityKit
import UIKit

#if targetEnvironment(simulator)
#else
extension ARViewController {
    func billboardGestureSetup(_ billboard: BillboardEntity) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panOnBillboard))
        billboard.view?.addGestureRecognizer(panGesture)
    }
    
    //- Tag: PanOnBillboard
    fileprivate func panBillboard(_ sender: UIPanGestureRecognizer, _ billboardView: BillBoardView, _ panLocation: CGPoint) {
        
        let feedbackGenerator = UIImpactFeedbackGenerator()
        
        switch sender.state {
        case .began:
            // Prepare the taptic engine to reduce latency in delivering feedback.
            feedbackGenerator.prepare()
            
            // Drag if the gesture is beginning.
            billboardView.stickyNote.isDragging = true
            
            // Save offsets to implement smooth panning.
            guard let frame = sender.view?.frame else { return }
            billboardView.xOffset = panLocation.x - frame.origin.x
            billboardView.yOffset = panLocation.y - frame.origin.y
            
            // Fade in the widget that's used to delete sticky notes.
            trashZone.fadeIn(duration: 0.4)
        case .ended:
            // Stop dragging if the gesture is ending.
            billboardView.stickyNote.isDragging = false
            
            // Delete the sticky note if the gesture ended on the trash widget.
            if billboardView.isInTrashZone {
                deleteBillboard(billboardView.stickyNote)
                // ...
            } else {
                attemptRepositioning(billboardView)
            }
            
            // Fades out the widget that's used to delete sticky notes when there are no sticky notes currently being dragged.
            if !billboards.contains(where: { $0.isDragging }) {
                trashZone.fadeOut(duration: 0.2)
            }
        default:
            // This case is for state = .changed
            // Update the sticky note's screen position based on the pan location, and initial offset.
            billboardView.frame.origin.x = panLocation.x - billboardView.xOffset
            billboardView.frame.origin.y = panLocation.y - billboardView.yOffset
            
            // We update playgroundEntity's reckoning of whether it's in the trash zone.
            billboardView.isInTrashZone = trashZone.frame.contains(panLocation)
        }
    }
    
    /// Sticky note pan-gesture handler.
    /// - Tag: PanHandler
    @objc
    func panOnBillboard(_ sender: UIPanGestureRecognizer) {
        
        guard let stickyView = sender.view as? BillBoardView else { return }
        
        let panLocation = sender.location(in: arView)
        
        // Ignore the pan if any StickyViews are being edited.
        for note in billboards where note.isEditing { return }
        
        panBillboard(sender, stickyView, panLocation)
    }
    
    func deleteBillboard(_ billboard: BillboardEntity) {
        guard let index = billboards.firstIndex(of: billboard) else { return }
        billboard.removeFromParent()
        billboards.remove(at: index)
        billboard.view?.removeFromSuperview()
        billboard.view?.isInTrashZone = false
    }
    
    /// - Tag: AttemptRepositioning
    fileprivate func attemptRepositioning(_ billboardView: BillBoardView) {
        let point = CGPoint(x: billboardView.frame.midX, y: billboardView.frame.midY)
        
        let result = arView.unproject(point, ontoPlane: billboardView.stickyNote.transform.matrix)
        
        if let result = result {
            billboardView.stickyNote.transform.translation = result
        } else {
            billboardView.stickyNote.shouldAnimate = true
        }
        
        // This sample implementation by Apple was overriden by us to mimic the behavior of
        // the default *entity* gestures instead (do not look for plane on reposition, simply drag on existing plane)
        
        // Conducts a ray-cast for feature points using the panned position of the StickyNoteView
//        if let result = arView.raycast(from: point, allowing: .estimatedPlane, alignment: .any).first {
//            billboardView.stickyNote.transform.matrix = result.worldTransform
//        } else {
//            billboardView.stickyNote.shouldAnimate = true
//        }
        
       
    }
}

#endif
