//
//  PlaygroundEntity.swift
//  siemob
//
//  Created by Yifeng Dong on 03.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import RealityKit

class PlaygroundEntity: Entity, HasCollision, HasPlaygroundItem, Codable {
    // These variables are not transmitted over MultipeerService.
    // They are instantiated to their default value on each device.
    var isInTrashZone = false
    var entitySetupDone = false
    
    init(item: PlaygroundItem) {
        super.init()
        // Only Components set this way are transmitted through MultipeerService
        self.components[PlaygroundItemComponent] = PlaygroundItemComponent(item: item)
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let item = try values.decode(PlaygroundItem.self, forKey: .item)
        
        // Only Components set this way are transmitted through MultipeerService
        self.components[PlaygroundItemComponent] = PlaygroundItemComponent(item: item)
    }
    
    required init() {
        super.init()
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(item, forKey: .item)
    }

    
    #if targetEnvironment(simulator)
    #else

    public func setupEntity(inView arView: WorldView) {
        guard let loadedEntity = item?.load() else {
            return
        }
        
        self.children.removeAll()
        
        // Add the loaded model as a child of playgroundEntity
        self.addChild(loadedEntity)
        
        // Create the collision model of our Entity
        let entityBounds = loadedEntity.visualBounds(relativeTo: self)
        self.collision = CollisionComponent(shapes: [ShapeResource.generateBox(size: entityBounds.extents).offsetBy(translation: entityBounds.center)])
        
        // Install default gestures on the new object
        let installedGestures = arView.installGestures(.all, for: self)
        
        // Insert our own actions for the created GestureRecognizers
        // See documentation for handle.TranslationToDelete for more info.
        for gestureRecognizer in installedGestures {
            gestureRecognizer.addTarget(arView.parentViewController!,
                                        action: #selector(arView.parentViewController.requestOwnershipOnGesture(_:)))
            
            if gestureRecognizer is EntityTranslationGestureRecognizer {
                gestureRecognizer.addTarget(arView.parentViewController!,
                                        action: #selector(arView.parentViewController.handleTranslationToDelete(_:)))
            }
        }
        
        entitySetupDone = true
    }
    #endif

    enum CodingKeys: String, CodingKey {
        case item
    }
}
