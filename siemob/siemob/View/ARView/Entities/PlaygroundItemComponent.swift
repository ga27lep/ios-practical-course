//
//  PlaygroundItemComponent.swift
//  siemob
//
//  Created by Yifeng Dong on 26.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import RealityKit

protocol HasPlaygroundItem { }

extension HasPlaygroundItem where Self: Entity {
    var playgroundItemComponent: PlaygroundItemComponent? {
        get {
            components[PlaygroundItemComponent]
        }
        
        set {
            components[PlaygroundItemComponent] = newValue
        }
    }
    
    var item: PlaygroundItem? {
        get {
            playgroundItemComponent?.item
        }
        
        set {
            playgroundItemComponent?.item = newValue
        }
    }
}

// Components can be transmitted automatically through MultipeerService
struct PlaygroundItemComponent: Component, Codable {
    var item: PlaygroundItem?
}
