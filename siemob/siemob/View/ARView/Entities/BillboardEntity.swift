/*
See LICENSE folder for this sample’s licensing information.

Abstract:
An entity used to house the AR screen space annotation.
*/

import ARKit
import RealityKit

/// An Entity which has an anchoring component and a screen space view component, where the screen space view is a BillBoardView.
class BillboardEntity: Entity, HasAnchoring, HasScreenSpaceView {

    var screenSpaceComponent = ScreenSpaceComponent()
    
    /// The original dimensions of the billboard
    var originalSize = CGSize(width: 200, height: 200)
    
    /// Initializes a new BillboardEntity and assigns the specified transform.
    /// Also automatically initializes an associated BillboardView with the specified frame.
    init(frame: CGRect, worldTransform: simd_float4x4) {
        super.init()
        self.transform.matrix = worldTransform
        self.originalSize = frame.size
        // ...
        screenSpaceComponent.view = BillBoardView(frame: frame, note: self)
    }
    
    required init() {
        super.init()
    }
}
