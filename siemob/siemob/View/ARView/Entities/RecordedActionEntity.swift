//
//  RecordedActionEntity.swift
//  siemob
//
//  Created by Yifeng Dong on 09.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

#if targetEnvironment(simulator)
#else

import Foundation
import RealityKit
import ARKit

class RecordedActionEntity: PlaygroundEntity {
    var playing = true
    var frame = 0
    
    var animationFrames: [[simd_float4x4]] {
        (self.item as? RecordedAction)?.jointFrames ?? []
    }
    
    var rootTranslations: [simd_float3] {
        (self.item as? RecordedAction)?.rootTranslations ?? []
    }
    
    var rootRotations: [simd_quatf] {
        (self.item as? RecordedAction)?.rootRotations ?? []
    }
    
    init(fromAction action: RecordedAction) {
        super.init(item: action)
    }
    
    
    required convenience init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let item = try values.decode(RecordedAction.self, forKey: .item)
        
        self.init(fromAction: item)
    }
    
    required init() {
        super.init()
    }
}


#endif
