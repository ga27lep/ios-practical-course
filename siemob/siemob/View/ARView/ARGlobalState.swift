//
//  ARGlobalState.swift
//  siemob
//
//  Created by Yifeng Dong on 06.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation

class ARGlobalState: ObservableObject {
    @Published var selectedItemToAdd: PlaygroundItem?
    @Published var simulation: Bool = false
    @Published var recording: Bool = false
    @Published var pressedSimulationButton = false
    
 //  @Published var
    
    private static var instances = 0
    
    init() {
        assert(Self.instances == 0)
        Self.instances += 1
    }
}
