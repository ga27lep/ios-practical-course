//
//  GradientView.swift
//  siemob
//
//  Created by Yifeng Dong on 05.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import UIKit

/// Tag: Convenient class to make a simple top/bottom gradient view\
/// Credits: Apple Sample Project

class GradientView: UIView {
    
    init(topColor: CGColor, bottomColor: CGColor) {
        super.init(frame: .zero)
        let gradientLayer = layer as? CAGradientLayer
        gradientLayer?.colors = [
            topColor,
            bottomColor
        ]
        backgroundColor = .clear
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
}
