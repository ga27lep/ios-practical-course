//
//  ARVIewController.swift
//  siemob
//
//  Created by Yifeng Dong on 04.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

#if targetEnvironment(simulator)
#else
import Foundation
import UIKit
import RealityKit
import ARKit
import Combine
import SwiftUI
import MultipeerConnectivity

/// UIKit ViewController for the AR view. All AR-related stuff should go here.
class ARViewController: UIViewController {
    
    var arView: WorldView!
    var trashZone: UIView!
    var deleteLabel: UIView!
    var shadeView: UIView!
    
    var keyboardHeight: CGFloat!
    
    var arGlobalState: ARGlobalState!
    var model: Model!
    
    var objectToAdd: PlaygroundItem?
    
    var recordedActionEntities: [RecordedActionEntity] = []
    
    var recordingAnchor = AnchorEntity()
    let characterOffset: SIMD3<Float> = [-1.0, 0, 0] // Offset the character by one meter to the left
    var recordingEntity: BodyTrackedEntity?
    var newRecordedAction = RecordedAction(name: "Action", jointFrames: [])
    var recordingStateLastFrame = false
    
    var recordingStartPosition: Transform? // The starting position of the root node in a recording
    var recordingLastPosition: simd_float3?
    
    var cancel: AnyCancellable?
    
    
    var subscription: Cancellable!
    
    var billboards: [BillboardEntity] = []
    
    
    var drawing: DrawingState!
    
    //    drawing try
    var sceneObserver: Cancellable!
    var cameraTransform: Transform!
    var anchorsDrawing = [Int: [AnchorEntity]]()
    var helper = [AnchorEntity]()
    var fenceAnchors = [AnchorEntity]()
    var counter = 0
    // Multipeer
    var multipeerHelper: MultipeerHelper?
    
    
    // Called after the controller's view is loaded into memory.
    // Initialization and setup should go here.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize Views and add them to the ViewController's views
        
        // The main view of this ViewController is arView.
        // arView contains all of the (low-level) RealityKit functionality.
        // arView is of type WorldView, which is our custom subclass of Apple's default class ARView
        arView = WorldView(frame: .zero)
        arView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(arView)
        NSLayoutConstraint.activate([
            arView.topAnchor.constraint(equalTo: self.view.topAnchor),
            arView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            arView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            arView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
        
        // Some of our User-Interaction is in this ViewController.
        // Other (more low-level) parts are handled in arView directly.
        // arView needs to get the state information it needs to perform those actions.
        arView.arGlobalState = arGlobalState
        arView.objectToAdd = objectToAdd
        arView.parentViewController = self
        
        PlaygroundItemComponent.registerComponent()
        
        //multipeerHelper = MultipeerHelper(serviceName: "siemob-test", sessionType: .both, delegate: self)
        self.setupCollaboration()
        arView.session.delegate = self
        
        subscription = arView.scene.subscribe(to: SceneEvents.Update.self) { [unowned self] in
            self.updateScene(on: $0)
        }
        
        shadeView = UIView(frame: .zero)
        shadeView.translatesAutoresizingMaskIntoConstraints = false
        arView.addSubview(shadeView)
        NSLayoutConstraint.activate([
            shadeView.topAnchor.constraint(equalTo: arView.topAnchor),
            shadeView.leadingAnchor.constraint(equalTo: arView.leadingAnchor),
            shadeView.trailingAnchor.constraint(equalTo: arView.trailingAnchor),
            shadeView.bottomAnchor.constraint(equalTo: arView.bottomAnchor)
        ])
        shadeView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        shadeView.alpha = 0
        arView.drawing = drawing
       // arView.addCoaching()
        sceneObserver = arView.scene.subscribe(to: SceneEvents.Update.self) { [unowned self] in self.updateScene(on: $0) }
        
        // Setup the trash zone with its "Delete" label.
        setupTrashZone()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Add observer to the keyboardWillShowNotification to get the height of the keyboard every time it is shown
        let notificationName = UIResponder.keyboardWillShowNotification
        let selector = #selector(keyboardIsPoppingUp(notification:))
        NotificationCenter.default.addObserver(self, selector: selector, name: notificationName, object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Body Tracking Configuration can only be run when an actual recording is
        // started, otherwise it will disable ARWorldTrackingConfiguration and cause
        // collaborative sessions to not work.
        
        self.recordingAnchor = AnchorEntity()
        
        arView.scene.addAnchor(recordingAnchor)
        
        // Asynchronously load the 3D character.
        cancel = Entity.loadBodyTrackedAsync(named: "robot").sink(
            receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    print("Error: Unable to load model: \(error.localizedDescription)")
                }
                self.cancel?.cancel()
        }, receiveValue: { (character: Entity) in
            if let character = character as? BodyTrackedEntity {
                // Scale the character to human size
                character.scale = [1.0, 1.0, 1.0]
                self.recordingEntity = character
                self.cancel?.cancel()
            } else {
                print("Error: Unable to load model as BodyTrackedEntity")
            }
        })
    }
    }
// MARK: - Extension: Gestures
extension ARViewController {
    /// This function is called when an Entity is dragged by the user (EntityTranslationGestureRecognizer).
    /// It implements the functionality for deleting objects.
    ///
    /// When an Entity is added (functionality is in arView),
    /// the default Entity gestures are installed in it (dragging, rotating, etc.,)
    /// in the form of UIGestureRecognizers of the type EntityGestureRecognizer.
    ///
    /// In UIKit, GestureRecognizers are assigned "actions" which fire when the GestureRecognizer detects
    /// the gesture by the user. Apple's default entity gesture installation handles all of the normal
    /// tasks relating to moving objects around.
    ///
    /// Here we define a *custom, second* action to be fired whenver an EntityTranslationGestureRecognizer's state is updated,
    /// in addition to the default behavior. The default behavior is not overriden by this.
    ///
    /// This action is associated with the concrete GestureRecognizer of each Entity when the Entity is created
    /// in arView.
    ///
    /// This code is mostly taken from Apple's sample project for screen space in RealityKit.
    @objc
    func handleTranslationToDelete(_ sender: UIGestureRecognizer) {
        guard let sender = sender as? EntityTranslationGestureRecognizer else {
            return
        }
        
        guard let playgroundEntity = sender.entity as? PlaygroundEntity else {
            return
        }
        
        // Taptic Feedback for when the user crosses the trash Zone treshold.
        // This isn't fully implemented here yet, so it doesn't work.
        let feedbackGenerator = UIImpactFeedbackGenerator()
        
        // UIGestureRecognizers have a couple of main states: Began, Changed, and Ended, among others.
        // This function could be called with any of those states, so we have to switch.
        switch sender.state {
        case .began:
            // Prepare the taptic engine to reduce latency in delivering feedback.
            feedbackGenerator.prepare()
            
            // Fade in the widget that's used to delete sticky notes.
            trashZone.fadeIn(duration: 0.4)
        case .ended:
            // Remove entity if it's in the trash zone
            if playgroundEntity.isInTrashZone, let objectAnchor = sender.entity?.anchor {
                print(trashZone.frame.debugDescription)
                arView.scene.removeAnchor(objectAnchor)
            }
            
            trashZone.fadeOut(duration: 0.2)
        default:
            // This case is for state = .changed
            // We update playgroundEntity's reckoning of whether it's in the trash zone.
            
            playgroundEntity.isInTrashZone = trashZone.frame.contains(sender.location(in: arView))
        }
        
        print(sender.description)
        print(sender.location(in: arView))
    }
    
   /// Requests ownership of the object when an EntityGesture starts
    @objc
    func requestOwnershipOnGesture(_ sender: UIGestureRecognizer) {
        guard let sender = sender as? EntityGestureRecognizer else { return }
        
        guard let entity = sender.entity else { return }
        
        if sender.state == .began, !entity.isOwner {
            entity.requestOwnership({ _ in })
        }
    }
}

// MARK: Extension: Setup
extension ARViewController {
    /// Adds the trashZone to arView as a subview.
    fileprivate func setupTrashZone() {
        trashZone = GradientView(topColor: UIColor.red.withAlphaComponent(0.7).cgColor, bottomColor: UIColor.red.withAlphaComponent(0).cgColor)
        trashZone.translatesAutoresizingMaskIntoConstraints = false
        
        arView.addSubview(trashZone)
        
        NSLayoutConstraint.activate([
            trashZone.topAnchor.constraint(equalTo: arView.topAnchor),
            trashZone.leadingAnchor.constraint(equalTo: arView.leadingAnchor),
            trashZone.trailingAnchor.constraint(equalTo: arView.trailingAnchor),
            trashZone.heightAnchor.constraint(equalTo: arView.heightAnchor, multiplier: 0.2)
        ])
        
        trashZone.alpha = 0
        addDeleteLabel()
    }
    
    /// Adding a Delete label to the trashZone for clarity
    fileprivate func addDeleteLabel() {
        let deleteLabel = UILabel()
        deleteLabel.translatesAutoresizingMaskIntoConstraints = false
        
        trashZone.addSubview(deleteLabel)
        
        NSLayoutConstraint.activate([
            deleteLabel.topAnchor.constraint(equalTo: trashZone.safeAreaLayoutGuide.topAnchor, constant: 40),
            deleteLabel.centerXAnchor.constraint(equalTo: trashZone.centerXAnchor)
        ])
        
        deleteLabel.text = "Delete"
        deleteLabel.textColor = .white
    }
}


extension ARViewController: ARSessionDelegate {
    func updateScene(on event: SceneEvents.Update) {
        
        /// Setup Entities added by MultipeerService
        for anchorEntity in arView.scene.anchors {
            for playgroundEntity in anchorEntity.children.compactMap({ $0 as? PlaygroundEntity}) where !playgroundEntity.entitySetupDone {
                
                playgroundEntity.setupEntity(inView: arView)
                //anchorEntity.anchor?.reanchor(.plane(.any, classification: .any, minimumBounds: [0, 0]), preservingWorldTransform: true)
            }
        }
        
        /// Billboards:
        
        let billboardsToUpdate = billboards.compactMap { !$0.isEditing && !$0.isDragging ? $0 : nil }
        for billboard in billboardsToUpdate {
            // Gets the 2D screen point of the 3D world point.
            guard let projectedPoint = arView.project(billboard.position) else { return }
            
            // Calculates whether the note can be currently visible by the camera.
            let cameraForward = arView.cameraTransform.matrix.columns.2.xyz
            let cameraToWorldPointDirection = normalize(billboard.transform.translation - arView.cameraTransform.translation)
            let dotProduct = dot(cameraForward, cameraToWorldPointDirection)
            let isVisible = dotProduct < 0
            
            // Updates the screen position of the note based on its visibility
            billboard.projection = Projection(projectedPoint: projectedPoint, isVisible: isVisible)
            billboard.updateScreenPosition()
            
            // Scale the billboard according to distance
            let dist = CGFloat(distance(arView.cameraTransform.translation, billboard.transform.translation))
            let newSize = CGSize(width: billboard.originalSize.width / dist,
                                 height: billboard.originalSize.height / dist)
            
            guard let view = billboard.view else { return }
            
            view.frame = CGRect(origin: view.frame.origin, size: newSize)
            
        }
        
        /// Drawing implementation:
        
        if drawing.drawingMode {
            
            
            let tranform = arView.cameraTransform.matrix.inverse
            let orientation = SIMD3(x: -tranform.columns.0.z, y: -tranform.columns.1.z, z: -tranform.columns.2.z)
            let location = SIMD3(x: tranform.columns.0.w, y: tranform.columns.1.w, z: tranform.columns.2.w)
            let currentPosition = orientation + location
            let drawAnchor = AnchorEntity(world: currentPosition)
            
            
            if drawing.tappedDraw {
                counter = counter + 1
                if counter % 9 == 0 {
                let drawNode = MeshResource.generateBox(size: self.drawing.lineWidthDrawing)
                let material = UnlitMaterial(color: drawing.colorDraw)
                let entity = ModelEntity(mesh: drawNode, materials: [material])
                drawAnchor.addChild(entity)
                helper.append(drawAnchor)
                arView.scene.anchors.append(drawAnchor)
                anchorsDrawing[self.drawing.drawCounter] = helper
                }
            }
            
            if drawing.isErase {
                for anchor in helper {
                    arView.scene.anchors.remove(anchor)
                    
                }
                for anchor in fenceAnchors {
                    arView.scene.anchors.remove(anchor)
                    
                }
            }
            
            if drawing.drawFence {
                let fenceAnchor = AnchorEntity(plane: .horizontal)
                 let drawNode = MeshResource.generateBox(size: self.drawing.lineWidthFence)
                let material = UnlitMaterial(color: drawing.colorFence)
                let entity = ModelEntity(mesh: drawNode, materials: [material])
                fenceAnchor.addChild(entity)
                fenceAnchors.append(fenceAnchor)
                arView.scene.anchors.append(fenceAnchor)
            }
        }
    }

    
    /*
     
     Extensions for collaborative session
     */
    
    // Periodically called by ARKit, whenever collaboration data is available; share collaboration data
    func session(_ session: ARSession, didOutputCollaborationData data: ARSession.CollaborationData) {
        guard let multipeerHelper = multipeerHelper else { return }
        if !multipeerHelper.connectedPeers.isEmpty {
            guard let encodedData = try? NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: true)
                else { fatalError("Unexpectedly failed to encode collaboration data.") }
            multipeerHelper.sendToAllPeers(encodedData, reliably: true)
            print("DEBUG: Send collaboration data")
        } else {
            print("Deferred sending collaboration to later because there are no peers.")
        }
    }
    
    // New anchor has been added, assign model
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        for anchor in anchors {
            if let participantAnchor = anchor as? ARParticipantAnchor {
                // Participant Anchor added -> Peer has connected
                
                /*
                 Add participant anchor indicator ball
                 */
                let anchorEntity = AnchorEntity(anchor: participantAnchor)
                let coloredSphere = ModelEntity(mesh: MeshResource.generateSphere(radius: 0.03),
                                                materials: [SimpleMaterial(color: .green, isMetallic: true)])
                anchorEntity.addChild(coloredSphere)
                
                arView.scene.addAnchor(anchorEntity)
                
                print("Participant anchor added")
                
                /// Re-add ARAnchors
                for anchor in session.currentFrame?.anchors ?? []
                    where !(anchor is ARParticipantAnchor) {
                        session.remove(anchor: anchor)
                        session.add(anchor: anchor)
                }
            }
        }
    }
    
    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        if arGlobalState.recording {
            for anchor in anchors {
                guard let bodyAnchor = anchor as? ARBodyAnchor else { continue }
                
                // Update the position of the character anchor's position.
                let bodyPosition = simd_make_float3(bodyAnchor.transform.columns.3)
                recordingAnchor.position = bodyPosition + characterOffset
                // Also copy over the rotation of the body anchor, because the skeleton's pose
                // in the world is relative to the body anchor's rotation.
                recordingAnchor.orientation = Transform(matrix: bodyAnchor.transform).rotation
                
                // BEGIN RECORDING
                if let recordingEntity = recordingEntity, recordingEntity.parent == nil {
                    // Attach the character to its anchor as soon as
                    // 1. the body anchor was detected and
                    // 2. the character was loaded.
                    recordingAnchor.addChild(recordingEntity)
                    
                    newRecordedAction = RecordedAction(name: "Action", jointFrames: [])
                    self.recordingStartPosition = Transform(matrix: bodyAnchor.transform)
                    self.recordingLastPosition = bodyPosition
                }
            }
        }
    }

    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        if arGlobalState.recording {
            if !(arView.session.configuration is ARBodyTrackingConfiguration) {
                // Setup the body tracking configuration
                
                if ARBodyTrackingConfiguration.isSupported {
                    // Run a body tracking configration.
                    let configuration = ARBodyTrackingConfiguration()
                    arView.session.run(configuration)
                } else {
                    print("Body Tracking is only supported on devices with an A12 chip")
                }
            }
            
            for anchor in frame.anchors {
                guard let bodyAnchor = anchor as? ARBodyAnchor else { continue }
                
                let jointLocalPositions = bodyAnchor.skeleton.jointLocalTransforms
                self.newRecordedAction.jointFrames.append(jointLocalPositions)
                
                guard let recordingStartTransform = recordingStartPosition else { continue }
                // guard let recordingLastPosition = recordingLastPosition else { return }
                
                
                let bodyPosition = simd_make_float3(bodyAnchor.transform.columns.3)
                let rootTranslation = bodyPosition - recordingStartTransform.translation
                
                let rootRotation = Transform(matrix: bodyAnchor.transform).rotation
                
                self.newRecordedAction.rootTranslations.append(rootTranslation)
                self.newRecordedAction.rootRotations.append(rootRotation)
            }
        }
        
        // END RECORDING
        if !arGlobalState.recording && recordingStateLastFrame {
            if let recordingEntity = recordingEntity {
                recordingAnchor.removeChild(recordingEntity)
            }
            
            if !newRecordedAction.jointFrames.isEmpty {
                model.library.items.append(newRecordedAction)
            }
            
            // Stop ARBodyTrackingConfiguration and restart ARWorldTrackingConfiguration
            let configuration = ARWorldTrackingConfiguration()
            configuration.isCollaborationEnabled = true
            
            arView.session.run(configuration)
            
        }
        
        
        for character in recordedActionEntities where character.playing {
            let jointFrames = character.animationFrames
            let translationFrames = character.rootTranslations
            let rotationFrames = character.rootRotations
            
            let bodyTrackedEntity = character
                .children
                .compactMap({ $0 as? BodyTrackedEntity })
                .first
            
            bodyTrackedEntity?.bodyTracking.isPaused = true
            
            bodyTrackedEntity?.jointTransforms = jointFrames[character.frame % jointFrames.count].map({ pos in
                Transform(matrix: pos)
            })
            
            if !translationFrames.isEmpty {
                //character.setTransformMatrix(rootFrames[character.frame % rootFrames.count], relativeTo: character.parent)
                
                bodyTrackedEntity?.setPosition(
                    [0, translationFrames[character.frame % translationFrames.count].y, 0],
                    relativeTo: bodyTrackedEntity?.parent)
                
                //                print([character.frame % translationFrames.count])
                //                print(bodyTrackedEntity?.position(relativeTo: bodyTrackedEntity?.parent))
            }
            
            character.frame += 1
        }
        
        recordingStateLastFrame = arGlobalState.recording
    }
    
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        return true
    }
}

/*
 
 Extensions for collaborative session
 */

extension ARViewController: MultipeerHelperDelegate {
    func receivedData(_ data: Data, _ peer: MCPeerID) {
        if let collaborationData = try? NSKeyedUnarchiver.unarchivedObject(ofClass: ARSession.CollaborationData.self, from: data) {
            arView.session.update(with: collaborationData)
                    print("DEBUG: Received collaboration data")
                    return
                }
    }
}

extension ARViewController {
    func setupCollaboration() {
        guard let multipeerHelper = self.multipeerHelper else {
            print("FATAL: multipeerHelper NIL when initializing ARViewController")
            return
        }
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.isCollaborationEnabled = true
        
        arView.session.run(configuration)
        
        arView.scene.synchronizationService = self.multipeerHelper?.syncService
    }
}
#endif
