//
//  SimulationState.swift
//  siemob
//
//  Created by Lea Stoica on 04/01/2020.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation

class SimulationState: ObservableObject {
    @Published var simulation: Bool = false
}
