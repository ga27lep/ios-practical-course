//
//  MainView.swift
//  siemob
//
//  Created by Simon Dabrowski on 25.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

struct InitialView: View {
    @EnvironmentObject var model: Model

    var body: some View {
        VStack {
            if self.model.authenticated || self.model.collaborationOpened {
//                ProjectsView()
                ProfileView()
            } else {
                LoginView()
           }
        }
    }
}

struct InitialView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
