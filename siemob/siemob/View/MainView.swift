//
//  AllCameraView.swift
//  siemob
//
//  Created by Jass on 11/8/19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

/// Main SwiftUI View.
/// This View is loaded after transitioning from the entry views
struct MainView: View {
    // MARK: - Environment Objects
    @EnvironmentObject var model: Model
    @EnvironmentObject var arGlobalState: ARGlobalState
    @EnvironmentObject var multipeerHelper: MultipeerHelper
    
    // MARK: - States
    @State var showStoryView: Bool = false
    @State var showObjectListView: Bool = false
    @State var showRandomFacts: Bool = true
    @State var objectListState = CGSize.zero
    @State var storyViewState = CGSize.zero
    @State var showSettings = true
    @State var factIndex = 1
    @State var factText = ""
    @State var factColor = Color.orange
    @State var counter = 0
    
    @State var count: Int = 1
    //@State var simulationButton: Bool = false
    
    
    let timer = Timer.publish(every: 10, on: .main, in: .common).autoconnect()
    
    // MARK: - Observed Objects
    
    @ObservedObject var drawing = DrawingState()
    @ObservedObject var settingsState = SettingsState()
    //settings modal
    @State private var showModal = false
    
    
    // MARK: - Body of the View
    var body: some View {
        
        ZStack {
            // On the lowest layer: The AR Camera View
            
            self.arView  .frame(height: UIScreen.main.bounds.height)
            
            
            
            // The two horizontal toolbars: LeftToolbar and Story
            HStack {
                if showObjectListView {
                    LeftToolbarView(selection: .objects, arGlobalState: arGlobalState)
                        .frame(width: 250).offset(x: objectListState.width).padding(16)
                        .padding([.horizontal, .top], 20)
                        .gesture(DragGesture(minimumDistance: 50).onChanged({ self.determineObjectListViewTranslation(translation: $0.translation) })
                            .onEnded({ _ in self.determineObjectListViewState() }))
                } else {
                    placeHolderRectangle2(toggleProperty: showObjectListView, refreshStories: false)
                }
                Spacer()
                if showStoryView { storyView }
                else { placeHolderRectangle(toggleProperty: showStoryView, refreshStories: false) }
            }
            
            // Top: Multipeer Indicator and Bottom: Simulation Button
            
            VStack {
                VStack {
                    if showRandomFacts && !showStoryView && !showObjectListView && settingsState.randomFacts {
                        randomFacts(showObjectListView: \._showObjectListView, showStoryView: \._showStoryView,
                                    rFView: \._showRandomFacts).padding([.vertical, .top])
                    }
                    
                    multiPeerIndicator
                }
                Spacer()
                HStack {
                    
                    if !showStoryView && settingsState.simulation && !showObjectListView && !drawing.drawingMode {
                        simulationButton
                    }
                    

                    Spacer()
                    
                    if !showModal {
                        NavigationLink(destination: ProfileView(), isActive: $settingsState.showProfile, label: {EmptyView()})
                    }
                    
                    if settingsState.drawing && !showStoryView  && !showObjectListView {
                        DrawingView(drawing: drawing)
                    }
                    
                    Spacer()
                    
                    if !showStoryView && !drawing.drawingMode{
                        Button(action: {self.showModal.toggle()} ) {
                            Image(systemName: "gear").resizable().frame(width: 50, height: 50).foregroundColor(.white)
                        }.sheet(isPresented: $showModal) {
                            SettingsView(settingsState: self.settingsState, showModal: self.$showModal)
                        }
                    }
                    
                }.padding(.bottom, 10).padding(.horizontal ,50)
                
                }.frame(height: UIScreen.main.bounds.height)
            
            
            
        }
        .navigationBarTitle("").navigationBarHidden(true).edgesIgnoringSafeArea(.all).onReceive(timer) { _ in
            self.changeFact()
        }
    }
    
    // MARK: - Subviews
    
    /// Creates the rectangle indicating a hidden view
    /// - Parameter toggleProperty: A key path indicating which variable to set to true if the rectangle is dragged on
    func placeHolderRectangle(toggleProperty: Bool, refreshStories: Bool) -> some View {
        Rectangle()
            .fill(Color.white)
            .frame(width: 10, height: 300)
            .cornerRadius(40)
            .opacity(0.5)
            .gesture(DragGesture(minimumDistance: 50)
                .onChanged { _ in self.showStoryView = true }
                .onEnded({_ in
                    self.model.loadStoriesFromServer()
                    self.showStoryView = false
                }))
    }
    
    func placeHolderRectangle2(toggleProperty: Bool, refreshStories: Bool) -> some View {
        Rectangle()
            .fill(Color.white)
            .frame(width: 10, height: 300)
            .cornerRadius(40)
            .opacity(0.5)
            .animation(.spring())
            .gesture(DragGesture(minimumDistance: 50)
                .onChanged { _ in self.showObjectListView = true }
                .onEnded({_ in
                    self.model.loadStoriesFromServer()
                    self.showObjectListView = false
                }))
    }
    
    func changeFact() {
        if(showStoryView || showObjectListView || drawing.drawingMode){
            return
        }
        findRandomFactText(factIndex: (factIndex + 1) % 13)
    }
    
    func getRandomFactFromWeb(index: Int) {
      
        guard let url = URL(string: "https://uselessfacts.jsph.pl/random.json?language=en") else {
            return
        }
        let request = URLRequest(url: url)
        _ = URLSession.shared.dataTask(with: request) { data, _, _ in
            guard let data = data else {
                return
            }
            
            do {
                let serverResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable: Any]
              let text = serverResponse?["text"] as? String
              DispatchQueue.main.async {
                self.constructRFView(text: text ?? "", color: Color.blue, index: index)
              }
            } catch {
                return
            }
        }
        .resume()
    }
    
    func getObliqueStrategyFromWeb(index: Int) {
        
          guard let url = URL(string: "http://brianeno.needsyourhelp.org/draw") else {
              return
          }
          let request = URLRequest(url: url)
          _ = URLSession.shared.dataTask(with: request) { data, _, _ in
              guard let data = data else {
                  return
              }
              
              do {
                  let serverResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable: Any]
                let text = serverResponse?["strategy"] as? String
                  DispatchQueue.main.async {
                    self.constructRFView(text: text ?? "", color: Color.green, index: index)
                  }
              } catch {
                  return
              }
          }
          .resume()
    }
    
    func findRandomFactText(factIndex: Int) {
        if factIndex == 0 {
            constructRFView(text: " Put some objects from the list to make the playground better! ", color: Color.orange, index: factIndex)
        } else if factIndex == 6 {
            constructRFView(text: " Stuck in the list? Create a story to have a better idea! ", color: Color.orange, index: factIndex)
        } else if factIndex % 4 == 0 {
            getObliqueStrategyFromWeb(index: factIndex)
        } else {
            getRandomFactFromWeb(index: factIndex)
        }
    }
    
    func constructRFView(text: String,color: Color, index: Int){
        factText = text
        factColor = color
        factIndex = index
    }
    
    func randomFacts(showObjectListView: WritableKeyPath<Self, State<Bool>>, showStoryView: WritableKeyPath<Self, State<Bool>>,
                     rFView: WritableKeyPath<Self, State<Bool>>) -> some View {
        return VStack {
            if self.factIndex % 2 == 1 {
                EmptyView()
            } else {
                if self.factIndex == 0 {
                    Text(factText)
                    .frame(maxWidth: 500, minHeight: 50, alignment: .center)
                    .foregroundColor(.white)
                    .background(factColor)
                    .opacity(0.7)
                    .cornerRadius(25).onTapGesture {
                        self[keyPath: showObjectListView].wrappedValue = true
                    }
                } else if self.factIndex == 6 {
                    Text(factText)
                    .frame(maxWidth: 500, minHeight: 50, alignment: .center)
                    .foregroundColor(.white)
                    .background(factColor)
                    .opacity(0.7)
                    .cornerRadius(25).onTapGesture {
                        self[keyPath: showStoryView].wrappedValue = true
                    }
                } else {
                    Text(factText)
                    .frame(maxWidth: 500, minHeight: 50, alignment: .center)
                    .foregroundColor(.white)
                    .background(factColor)
                    .opacity(0.7)
                    .cornerRadius(25)
                }
            }
        }
    }
    
    
    
    /// Indicator for the state of Multipeer Session
    var multiPeerIndicator: some View {
        VStack {
            if !self.multipeerHelper.connectedPeers.isEmpty {
                Text("Connected devices:").bold()
            }
            
            ForEach(self.multipeerHelper.connectedPeers, id: \.self) { peerId in
                Text("\(peerId.displayName)")
            }
        }
    }
    
    /// Play button for simulation
    var instructionsSimulation: some View {
        VStack {
            if settingsState.simulation && counter == 0 {
                Text("You are in Simulation Mode. Tap to place Avatars. If you want to add Objects, we recommend closing Simulation Mode.Tap to start the Simulation Mode")
                    .frame(width: 470).foregroundColor(.white).cardButtonViewModifier(color: Color("gray")).padding(.horizontal)
            }
            
            if settingsState.simulation && counter == 1 {
                Text("Tap on the screen to delete Avatars.")
                    .frame(width: 350).foregroundColor(.white).cardButtonViewModifier(color: Color("gray")).padding(.horizontal)
            }
        }
    }
    
    var simulationButton: some View {
        HStack {
            //   instructionsSimulation
            Button(action: { self.arGlobalState.simulation.toggle(); self.arGlobalState.pressedSimulationButton = true; self.counter = self.counter + 1}) {
                if self.arGlobalState.simulation {
                    VStack {
                        Image(systemName: "pause").resizable().frame(width: 50, height: 50).foregroundColor(.green)
                    }
                } else {
                    Image(systemName: "play").resizable().frame(width: 50, height: 50).foregroundColor(.white)
                }
            }
        }
    }
    
    
    /// Alias for the main AR View. Wrapper for compiler instructions determining whether to show camera or mock.
    var arView: some View {
        #if targetEnvironment(simulator)
        return MockARView()
        #else
        return ARCameraView(drawing: drawing)
        
        #endif
    }
    
    // MARK: - Determining whether or not to show toolbars
    func determineObjectListViewState() {
        if self.objectListState.width < -100 {
            self.showObjectListView = false
        }
        self.objectListState = .zero
    }
    
    func determineStoryViewState() {
        if self.storyViewState.width > 300 {
            self.showStoryView = false
        }
        self.storyViewState = .zero
    }
    
    func determineObjectListViewTranslation(translation: CGSize) {
        if translation.width < 0 {
            self.objectListState = translation
        }
    }
    
    func determineStoryViewTranslation(translation: CGSize) {
        if translation.width > 0 {
            self.storyViewState = translation
        }
    }
    
    var storyView: some View {
        AllStoryView().padding(20)
            .offset(x: storyViewState.width)
            .frame(height: UIScreen.main.bounds.height)
            .gesture(DragGesture(minimumDistance: 50)
                .onChanged({ self.determineStoryViewTranslation(translation: $0.translation) })
                .onEnded({ _ in self.determineStoryViewState() }))
    }
}
struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView().environmentObject(Model.mock)
        //.previewLayout(.fixed(width: 1366, height: 1024))
    }
}
