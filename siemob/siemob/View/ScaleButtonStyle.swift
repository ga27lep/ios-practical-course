//
//  ScaleEffect.swift
//  siemob
//
//  Created by Jessica on 1/22/20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation
import SwiftUI

struct ScaleButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .scaleEffect(configuration.isPressed ? 1.1 : 1)
    }
}

