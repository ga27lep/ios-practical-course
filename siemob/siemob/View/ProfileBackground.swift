//
//  ProfileBackground.swift
//  siemob
//
//  Created by Jessica on 1/26/20.
//  Copyright © 2020 TUM. All rights reserved.
//
//
import SwiftUI

struct ProfileBackground: View {
     @EnvironmentObject var model: Model
    var body: some View {
      
        Image("silhouette").resizable().blur(radius: 0.9)
        
        }
    
}

struct ProfileBackground_Previews: PreviewProvider {
    static var previews: some View {
        ProfileBackground()
    }
}
