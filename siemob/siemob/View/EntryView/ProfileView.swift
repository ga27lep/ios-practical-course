//
//  ProfileView.swift
//  siemob
//
//  Created by Jessica on 1/26/20.
//  Copyright © 2020 TUM. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    
      @EnvironmentObject var model: Model
      @EnvironmentObject var multipeerHelper: MultipeerHelper
      @State private var presentMe = false
      @State private var name: String = ""
      @State private var isPresented: Bool = false

      @State private var joinNearbySessionPressed = false

      
    
    var body: some View {
        NavigationView {
        ZStack {
                     Color(.white)
            VStack {
            ProfileBackground()  .frame(height: 450)
                Spacer()
        VStack {
            CircleImage().offset(y: -150).padding(.bottom, -320)
            
                        VStack(alignment: .leading) {
                                   Text("Bernd Bruegge")
                                    .font(.title).foregroundColor(.black).bold()
                                   HStack(alignment: .top) {
                                       Text("Playground Designer")
                                           .font(.subheadline).foregroundColor(.black)
                                       Spacer()
                                       Text("Munich, Germany")
                                           .font(.subheadline).foregroundColor(.black)
                                   }
                               }
                    .padding()
            
         
            if !isPresented {
            HStack(alignment: .top, spacing: 10) {
                createProject;Spacer();loadProjButton
                Spacer();joinSession
            }.padding()
            }
            if isPresented {
                loadedProject
            }
        }.padding()
               
            HStack {
                if isPresented {
                   backButton
                }
                Spacer()
            logoutButton
                }
            Spacer()
            }.padding(.bottom, 200)
            }
            
    }.environment(\.horizontalSizeClass, .compact).scaledToFill()
    }
    
    var loadedProject: some View {

         ScrollView(.horizontal, showsIndicators: false) {
        
             HStack(alignment: .top) {
                 NavigationLink(destination: MainView(), isActive: $presentMe) { EmptyView() }
                 ForEach(model.projects) { project in
                     
                     Button(action: {
                         self.model.currentProjectID = project.id.uuidString
                         self.model.loadStoriesFromServer()
                        self.multipeerHelper.startHostingSession()
                         self.presentMe = true
                     }, label: {
                         Text(project.nameValue)
                             .bold().font(.title).minimumScaleFactor(4)
                             .frame(width: 200, height: 200)
                            .background(Image("multi2").resizable()).cornerRadius(30)
                         
                         
                     }).buttonStyle(ScaleButtonStyle())
                     
                 }
             }
         }
    }
    var logoutButton: some View {
        Button(action: {
            self.model.authenticated = false
            self.model.projects.removeAll()
            self.model.stories.removeAll()
        }, label: {
          Image(systemName: "arrow.left.square").resizable().frame(width: 40, height: 40).foregroundColor(.black).padding(50)
        })
    }
    var backButton: some View {
        Button(action: {
                               self.isPresented.toggle()
                                                                   }, label: {
                                                                     Image(systemName: "chevron.left").resizable().frame(width: 30, height: 30).foregroundColor(.black).padding(50)
                                                                   })
    }
    var joinSession: some View {
        NavigationLink(destination: MainView(), isActive: $joinNearbySessionPressed) {
            Button(action: {
                self.multipeerHelper.startJoiningSession()
                self.joinNearbySessionPressed = true
                self.model.collaborationOpened = true
            }, label: {
                Image(systemName: "person.3.fill").resizable().frame(width: 75, height: 45 ,alignment: .center).foregroundColor(.black).frame(width: 200, height: 200).background(Image("silhouette2").resizable().frame(width: 200, height: 200)).cornerRadius(30)
                
            }).buttonStyle(ScaleButtonStyle())
        }.buttonStyle(ScaleButtonStyle())

    }
    
    var createProject: some View {
        NavigationLink(destination: CreateProjectView()) {
                                      Image(systemName: "plus").resizable().frame(width: 50, height: 50 ,alignment: .center).foregroundColor(.black)
                                          .frame(width: 200, height: 200).background(Image("silhouette2").resizable().frame(width: 200, height: 200)).cornerRadius(30)
                              }.buttonStyle(ScaleButtonStyle())
                      
                     
    }
    
    var loadProjButton: some View {
        Button(action: {
                       self.isPresented.toggle()
                   }, label: {
                         Image(systemName: "square.grid.3x2.fill").resizable().frame(width: 75, height: 48 ,alignment: .center).foregroundColor(.black)
                                                     .frame(width: 200, height: 200).background(Image("silhouette2").resizable().frame(width: 200, height: 200)).cornerRadius(30)
                                       
                              
                   }).buttonStyle(ScaleButtonStyle())
                      
    }

}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
