//
//  ContentView.swift
//  Settings
//
//  Created by Jessica on 1/27/20.
//  Copyright © 2020 TUM. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    @ObservedObject var settingsState: SettingsState
    
    //sheet
      @Binding var showModal: Bool
      
    var body: some View {
       VStack {
        Image("Bruegge").resizable().frame(width: 250, height: 250)
                               .clipShape(Circle())
                               .overlay(
                                   Circle().stroke(Color.white, lineWidth: 4))
                               .shadow(radius: 10)
        
        Toggle(isOn: $settingsState.simulation) {
            Text("Animate the playground").bold()
            }.padding()
       
        Toggle(isOn: $settingsState.drawing) {
            Text("3D Drawing").bold()
                   }.padding()
        Toggle(isOn: $settingsState.randomFacts) {
                   Text("Random Facts").bold()
                          }.padding()
        Toggle(isOn: $settingsState.billboards) {
            Text("Billboard Annotations").bold()
                   }.padding()
    
        VStack(alignment: .center) {
            Button(action: {self.settingsState.showProfile = true; self.showModal = false }, label:{ Text("Exit Project")})
        }.offset( y: 30)
}
    }
}
//
//struct ContentView_Previews: PreviewProvider {
////    static var previews: some View {
////        SettingsView(settingsState: <#SettingsState#>, showModal: .constant(false))
////    }
//}

