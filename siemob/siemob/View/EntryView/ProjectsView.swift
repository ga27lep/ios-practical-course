//
//  ProjectsView.swift
//  siemob
//
//  Created by Simon Dabrowski on 05.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import SwiftUI

struct ProjectsView: View {
    @EnvironmentObject var model: Model
    @EnvironmentObject var multipeerService: MultipeerService
    
    @State private var presentMe = false
    
    var body: some View {
            VStack(alignment: .center, spacing: 10) {
                NavigationLink(destination: MainView(), isActive: $presentMe) { EmptyView() }
                
                ForEach(model.projects) { project in

                    Button(action: {
                        self.model.currentProjectID = project.id.uuidString
                        self.model.loadStoriesFromServer()
                        //self.multipeerService.startHostingSession() // TODO UNCOMMENT
                        self.presentMe = true
                    }, label: {
                        Text(project.nameValue)
                    })
                }
            }
    }
}

struct ProjectsView_Previews: PreviewProvider {
    static var previews: some View {
        ProjectsView()
    }
}
