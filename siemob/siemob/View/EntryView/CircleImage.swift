//
//  CircleImage.swift
//  siemob
//
//  Created by Jessica on 1/26/20.
//  Copyright © 2020 TUM. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    var body: some View {
        Image("Bruegge").resizable().frame(width: 250, height: 250)
                         .clipShape(Circle())
                         .overlay(
                             Circle().stroke(Color.white, lineWidth: 4))
                         .shadow(radius: 10)
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage()
    }
}
