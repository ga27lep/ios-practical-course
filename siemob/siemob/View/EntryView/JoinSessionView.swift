//
//  JoinSessionView.swift
//  siemob
//
//  Created by Dávid Endrédi on 06.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import SwiftUI
import MultipeerConnectivity

struct JoinSessionView: View {
    
    //@EnvironmentObject var multipeerService: MultipeerService
    private var multipeerService: MultipeerService
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var joinedSession = false
    @State var test = false
    
    init(multipeerService: MultipeerService) {
        self.multipeerService = multipeerService
    }
    
    var body: some View {
        
        NavigationView {
            VStack {
                NavigationLink(destination: MainView().environmentObject(Model.mock), isActive: $joinedSession) {
                    Text("")
                }
                JoinSessionViewContainer(multipeerService, self)
            }
        }.onAppear {
            self.joinedSession = false
        }
        .navigationBarBackButtonHidden(true)
    }
}

extension JoinSessionView: JoinSessionViewController {
    func didPressDoneButton() {
        joinedSession = true
    }
    
    func didPressCancelButton() {
        multipeerService.leaveSession()
        self.presentationMode.wrappedValue.dismiss()
    }
}

protocol JoinSessionViewController {
    func didPressCancelButton()
    func didPressDoneButton()
}

/*
 Wrapper class for MCBrowserViewController making it compatible with SwiftUI
 */
final class JoinSessionViewContainer: NSObject & UIViewControllerRepresentable {

    private var multipeerService: MultipeerService
    private var controller: JoinSessionViewController

    typealias UIViewControllerType = MCBrowserViewController
    var viewController: MCBrowserViewController

    public init(_ multipeerService: MultipeerService, _ controller: JoinSessionViewController) {
    //override public init() {
        self.multipeerService = multipeerService
        self.viewController = MCBrowserViewController(serviceType: MultipeerService.serviceType, session: multipeerService.session)
        self.controller = controller
        super.init()
        viewController.delegate = self
   }

    func makeUIViewController(context: UIViewControllerRepresentableContext<JoinSessionViewContainer>) -> MCBrowserViewController {
        return viewController
    }

    func updateUIViewController(_ uiViewController: MCBrowserViewController,
                                context: UIViewControllerRepresentableContext<JoinSessionViewContainer>) {
        // intentionally left blank
    }
}

extension JoinSessionViewContainer: MCBrowserViewControllerDelegate {
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        controller.didPressDoneButton()
    }

    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        controller.didPressCancelButton()
    }
}
