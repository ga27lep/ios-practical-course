//
//  CreateProjectView.swift
//  siemob
//
//  Created by Simon Dabrowski on 05.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import SwiftUI

struct CreateProjectView: View {
    @EnvironmentObject var model: Model
    @EnvironmentObject var multipeerHelper: MultipeerHelper
    
    @State private var name: String = ""
    @State private var presentMe = false
    let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0)

    var body: some View {
        
        NavigationView {
            ZStack {
                 Image("silhouette").resizable().blur(radius: 0.9)
                VStack {
                    Spacer()
                VStack(alignment: .leading) {
                    Text("Create new Project").bold().font(.largeTitle).foregroundColor(.black)
                    TextField("Project Name", text: $name).textFieldStyle(RoundedBorderTextFieldStyle())
                   }.padding(100)
                             
                HStack(alignment: .center) {
                NavigationLink(destination: MainView(), isActive: $presentMe) { EmptyView() }
                    VStack(alignment: .center) {
                Button(action: {
                   self.model.createProject(name: self.name)
                   self.multipeerHelper.startHostingSession()
                   self.presentMe = true
                }, label: {
                    Text("Let's go")
                        .foregroundColor(Color.white).bold().transition(.slide)
                           
                           .frame(maxWidth: 450, alignment: .center)
                           .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                           .background(Color.black)
                           
                        .cornerRadius(40)
                }).buttonStyle(ScaleButtonStyle())
                    }
                }
                    Spacer()
                }

            }
            } .navigationBarTitle("").navigationBarHidden(true).environment(\.horizontalSizeClass, .compact).scaledToFill()

    }
}
 
struct CreateProjectView_Previews: PreviewProvider {
    static var previews: some View {
        CreateProjectView()
    }
}
