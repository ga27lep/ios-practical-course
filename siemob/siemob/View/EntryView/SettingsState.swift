//
//  SettingsState.swift
//  siemob
//
//  Created by Jessica on 1/27/20.
//  Copyright © 2020 TUM. All rights reserved.
//

import Foundation


class SettingsState: ObservableObject {
    
    @Published var simulation: Bool = true
    @Published var drawing: Bool = true
    @Published var billboards: Bool = true
    @Published var randomFacts: Bool = true
    @Published var showProfile: Bool = false
    
    
    
}
