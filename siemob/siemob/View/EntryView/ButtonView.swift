//
//  ButtonView.swift
//  siemob
//
//  Created by Simon Dabrowski on 25.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

struct ButtonView: View {
    var text: String
    
    var body: some View {
        Text(text).font(.largeTitle)
            .foregroundColor(Color.white).bold()
               
               .frame(maxWidth: 450, alignment: .center)
               .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
               .background(Color.black)
               .cornerRadius(40)
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView(text: "Click Me")
    }
}
