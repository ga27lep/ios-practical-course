//
//  LoadingView.swift
//  siemob
//
//  Created by Jessica on 1/24/20.
//  Copyright © 2020 TUM. All rights reserved.
//

import SwiftUI

struct LoadingView<Content>: View where Content: View {

    @Binding var isShowing: Bool
    var content: () -> Content

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {

                self.content()
                    .disabled(self.isShowing)
                    .blur(radius: self.isShowing ? 3 : 0)

                VStack {
                    Text("Loading...").bold()
                    ActivityIndicator(isAnimating: .constant(true), style: .large).foregroundColor(Color.black)
                }
                .frame(width: 200,
                       height: 200)
                .foregroundColor(Color.black)
                .cornerRadius(20)
                .opacity(self.isShowing ? 1 : 0)

            }
        }
    }

}
