//
//  LoginView.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//


import SwiftUI
import Foundation

struct LoginView: View {
    
    @State private var username: String = "imagine-backend"
    @State private var password: String = "iM@gIn3_2o!9"
    @State private var loginbuttonLabel: String = "Login"
    @State private var success: Bool = false
    
    
    
    @EnvironmentObject var model: Model
    
    var body: some View {
        LoadingView(isShowing: $success) {
            ZStack {
                Image("silhouette").resizable().blur(radius: 0.9)
                VStack {
                    VStack {
                        
                        Image(systemName: "globe").resizable().aspectRatio(contentMode: ContentMode.fit)
                            .frame(width: 74.0, height: 74.0) .cornerRadius(10).padding(.bottom)
                            .foregroundColor(.black)
                        
                        Text("Login").bold().font(.title).padding(.bottom).foregroundColor(.black)
                        Text("Welcome Back!").font(.subheadline)
                            .padding(EdgeInsets(top: 0, leading: 0, bottom: 70, trailing: 0))  .foregroundColor(.black)
                        self.personUsername
                        Image("line").resizable().frame(width: 450, height: 3 ).background(Color(.black)).opacity(40)  .foregroundColor(.black)
                        VStack {
                            self.passwordView
                            Image("line").resizable().frame(width: 450, height: 3 ).background(Color(.black)).opacity(20)  .foregroundColor(.black)
                        }.padding()
                        self.loginButton
                        Text("\(self.model.invalidLoginText)").foregroundColor(Color.red)
                        
                        
                        
                    }.padding().background(Image("plane").resizable().cornerRadius(40).opacity(0.7))
                    
                }.padding(200)
            }
        }
    }
    
    
    var personUsername: some View {
        HStack {
            Image(systemName: "person.fill").resizable().frame(width: 25, height: 25)  .foregroundColor(.black)
            TextField("username", text: $username)
                .frame(width: 400, height: 70, alignment: .center)
                .cornerRadius(4.0)  .foregroundColor(.black)
        }
    }
    
    var passwordView: some View {
        HStack {
            Image(systemName: "lock.fill").resizable().frame(width: 25, height: 35)  .foregroundColor(.black)
            SecureField("password", text: $password) {
                // submit the password
            }.frame(width: 400, height: 70, alignment: .center)
                .cornerRadius(4.0)  .foregroundColor(.black)
        }
    }
    var loginButton: some View {
        Button(action: {
            self.model.loginAndRecieveToken(username: self.username, password: self.password)
            self.success.toggle()
        }, label: {
            Text(loginbuttonLabel)
                .foregroundColor(Color.white).bold().transition(.slide)
                
                .frame(maxWidth: 450, alignment: .center)
                .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                .background(Color.black)
                
                .cornerRadius(40)
            
        }).buttonStyle(ScaleButtonStyle())
    }
    
}
struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView().environmentObject(Model.mock)
    }
}


