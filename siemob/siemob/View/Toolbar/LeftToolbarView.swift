//
//  LeftToolbarView.swift
//  siemob
//
//  Created by Yifeng Dong on 15.11.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

struct LeftToolbarView: View {
    @State var selection: ItemLibrary.Collection
    @ObservedObject var arGlobalState: ARGlobalState
    @EnvironmentObject var model: Model
    
    var body: some View {
        VStack {
            Text("").frame( height: 150)
            Picker("Type", selection: $selection) {
                ForEach(ItemLibrary.Collection.allCases) { collection in
                    collection.icon.tag(collection)
                }
            }.pickerStyle(SegmentedPickerStyle())

            listView(selection: selection)
                .padding(.top, -40)
        }
        .background(Color.init(red: 0, green: 0, blue: 0, opacity: 0))
        .padding([.top], 30)
    }
    
    func listView(selection: ItemLibrary.Collection) -> some View {
        if selection == .actions {
            return AnyView(RecordedActionsPane())
        } else {
            return AnyView(ObjectList(category: selection))
        }
    }

    
}
struct LeftToolbarView_Previews: PreviewProvider {
    static var previews: some View {
        LeftToolbarView(selection: .objects, arGlobalState: ARGlobalState())
            .environmentObject(Model.mock)
    }
}
