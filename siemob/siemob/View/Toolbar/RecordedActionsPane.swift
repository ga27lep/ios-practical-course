//
//  RecordedActionsPane.swift
//  siemob
//
//  Created by Yifeng Dong on 10.01.20.
//  Copyright © 2020 TUM. All rights reserved.
//

import SwiftUI

struct RecordedActionsPane: View {
    @EnvironmentObject var arGlobalState: ARGlobalState
    @EnvironmentObject var model: Model
    
    var body: some View {
        VStack {
            recordButton.offset(y: 60)
            
            ObjectList(category: .actions, searchText: "").padding()
        }
    }
    
    /// Record Button
    var recordButton: some View {
        Button(action: { self.arGlobalState.recording.toggle() }) {
            if self.arGlobalState.recording {
                VStack {
                    Text("Recording").font(.headline).foregroundColor(.red)
                }
            } else {
                Text("Tap to Record Action").font(.headline).foregroundColor(.white)
            }
        }
    }
}

struct RecordedActionsPane_Previews: PreviewProvider {
    static var previews: some View {
        RecordedActionsPane()
    }
}
