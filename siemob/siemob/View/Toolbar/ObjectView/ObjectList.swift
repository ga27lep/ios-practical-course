//
//  ObjectList.swift
//  SiemobBeta
//
//  Created by Jass on 11/3/19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI
import Poly

struct ObjectList: View {
    @EnvironmentObject var model: Model
    @EnvironmentObject var arGlobalState: ARGlobalState
    var category: ItemLibrary.Collection = .objects
    @State var searchText: String = ""
    
    var body: some View {
        VStack {

            ScrollView {
                if !model.library.items.isEmpty {
                    ForEach(model.library.items(collection: category)) { item in
                        ObjectCell(id: item.id, selected: self.arGlobalState.selectedItemToAdd == item)
                            .frame(width: 200, height: 200)
                            .onTapGesture {
                                if self.arGlobalState.selectedItemToAdd == item {
                                    self.arGlobalState.selectedItemToAdd = nil
                                } else {
                                    self.arGlobalState.selectedItemToAdd = item
                                }
                            }
                    }
                } else {
                    VStack {
                        Text("LOADING...").foregroundColor(Color.blue)
                        ActivityIndicator(isAnimating: .constant(true), style: .large)
                    }
                }
            }.padding(50)
        }
    }
}

struct ObjectList_Previews: PreviewProvider {
    static var previews: some View {
        ObjectList(searchText: "").environmentObject(Model.mock)
            .previewLayout(.sizeThatFits)
    }
}

struct ActivityIndicator: UIViewRepresentable {
    
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style
  
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}
