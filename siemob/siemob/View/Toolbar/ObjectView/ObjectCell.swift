//
//  SwiftUIView.swift
//  SiemobBeta
//
//  Created by Jass on 11/3/19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

struct ObjectCell: View {
    @EnvironmentObject private var model: Model
    
    var id: PlaygroundItem.ID
    var selected: Bool
    
    var body: some View {
        model.library.item(byId: id).map({ object in
            VStack(alignment: .center, spacing: 10) {
                if object.thumbnail != "" {
                    Image(object.thumbnail ?? "").resizable().cornerRadius(15)
                }
                Text(object.name)
                    .font(.headline)
                    .foregroundColor(.white)
            }
            })
            .cardButtonViewModifier(color: selected ? Color.blue: Color("black"))
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static let mock = Model.mock
    static var previews: some View {
        ObjectCell(id: mock.library.items[2].id, selected: false)
            .frame(width: 200, height: 250)
            .previewLayout(.sizeThatFits)
            .environmentObject(mock)
    }
}
