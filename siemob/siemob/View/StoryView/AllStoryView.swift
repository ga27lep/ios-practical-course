//
//  StoryCell.swift
//  SiemobBeta
//
//  Created by Jass on 11/4/19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

struct AllStoryView: View {
    @EnvironmentObject private var model: Model
    
    @ObservedObject var speech = SpeechRecorder()
    
    @State var generateButtonPressed: Bool = false
    
    var body: some View {
        
        VStack {

            Text("Get inspired")
                .foregroundColor(Color.white)
                .bold()
                .font(.largeTitle)
                .padding()
            
            ScrollView(.horizontal, content: {
                HStack(spacing: 10) {
                    RecordStoryView(speech: speech, pressed: $generateButtonPressed)
                    ForEach(model.stories) { story in
                        StoryView(id: story.id)
                    }
                }.padding(.leading, 10)
            })
        }
            //.backgroundViewModifier()
            .cardButtonViewModifier(color: Color("black"))
            //.background(Color("black"))
            .frame(width: 600)
    }
}


struct AllStoryView_Previews: PreviewProvider {
    static var previews: some View {
        AllStoryView() .previewLayout(.fixed(width: 1366, height: 1024))
    }
}
