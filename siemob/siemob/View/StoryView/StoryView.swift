//
//  StoryCell.swift
//  SiemobBeta
//
//  Created by Jass on 11/4/19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

struct StoryView: View {
    @EnvironmentObject private var model: Model
    
    @State var id: Story.ID
    
    var body: some View {
        
        model.story(id).map { story in
            VStack(alignment: HorizontalAlignment.center, spacing: 0) {
                Text("Story")
                    .bold()
                    .font(.title)
                    .foregroundColor(Color.white)
                ScrollView {
                    (Text(story.textValue) + Text(story.storyValue).bold())
                        .foregroundColor(Color.white)
                        .padding(30)
                }
            }.frame(width: 300)
            .cardButtonViewModifier(color: Color("gray"))
        }
    }
}

struct StoryView_Previews: PreviewProvider {
    static let mock = Model.mock
    
    static var previews: some View {
        StoryView(id: mock.stories[0].id).previewLayout(.fixed(width: 1366, height: 512))
    }
}
