//
//  RecordStoryView.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.11.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import SwiftUI

struct RecordStoryView: View {
    @EnvironmentObject private var model: Model
    
    @ObservedObject var speech: SpeechRecorder
    @Binding var pressed: Bool
    @State var showingAlert: Bool = false
    var body: some View {
        VStack(alignment: .center, spacing: 50) {
            Text("Create a new story")
                .bold()
                .font(.title)
                .foregroundColor(.white)
            Button(action: {
                self.speech.toggleRecording()
            }) {
                if !self.speech.isRecording {
                    Image(systemName: "mic")
                        .font(.largeTitle)
                        .foregroundColor(.blue)
                } else {
                    Image(systemName: "mic")
                    .font(.largeTitle)
                    .foregroundColor(.red)
                }
            }
            //ScrollView {
                Text(speech.text).foregroundColor(.white).padding(30)
            //}
            Spacer()
            if !speech.text.isEmpty {
                VStack{
                    Button(action: {
                        self.saveStory(generate: false)
                    }) {
                        Text("Save Story").font(.title).foregroundColor(.white).padding(.bottom, 50)
                    }
                    Button(action: {
                        self.saveStory(generate: true)
                        self.showingAlert.toggle()
                    }) {
                        Text("Generate Story").font(.title).foregroundColor(.white).padding(.bottom, 50)
                    }.alert(isPresented: $showingAlert) {
                        Alert(title: Text(""), message: Text("Noted! That might take some time. Check back in a bit!"), dismissButton: .default(Text("Got it!")))
                    }
                }
            }
        }.frame(width: 300)
        .cardButtonViewModifier(color: Color("gray"))
    }
    
    func saveStory(generate: Bool) {
        self.pressed = true
        if self.speech.isRecording {
            self.speech.toggleRecording()
        }
        self.model.createStory(text: self.speech.text, generate: generate)
    }
}


struct RecordStoryView_Previews: PreviewProvider {
    static var previews: some View {
        RecordStoryView(speech: SpeechRecorder(), pressed: .constant(false))
    }
}
