

import MultipeerConnectivity
import RealityKit


public class MultipeerHelper: NSObject & ObservableObject {
  /// What type of session you want to make.
  ///
  /// `both` creates a session where all users are equal
  /// Otherwise if you want one specific user to be the host, choose `host` and `peer`
  public enum SessionType: Int {
    case host = 1
    case peer = 2
    case both = 3
    case manual = 4
  }
    
  @Published var connectedPeers = [MCPeerID]()

  public let sessionType: SessionType
  public let serviceName: String
  public var syncService: MultipeerConnectivityService? {
    if syncServiceRK == nil {
      syncServiceRK = try? MultipeerConnectivityService(session: session)
    }
    return syncServiceRK
  }

  public let myPeerID = MCPeerID(displayName: UIDevice.current.name)
  public private(set) var session: MCSession!
  public private(set) var serviceAdvertiser: MCNearbyServiceAdvertiser?
  public private(set) var serviceBrowser: MCNearbyServiceBrowser?
  private var syncServiceRK: MultipeerConnectivityService?

  public weak var delegate: MultipeerHelperDelegate?

  /// - Parameters:
  ///   - serviceName: name of the service to be added, must be less than 15 lowercase ascii characters
  ///   - sessionType: Type of session (host, peer, both)
  ///   - delegate: optional `MultipeerHelperDelegate` for MultipeerConnectivity callbacks
  public init(
    serviceName: String,
    sessionType: SessionType = .both,
    delegate: MultipeerHelperDelegate? = nil
  ) {
    self.serviceName = serviceName
    self.sessionType = sessionType
    self.delegate = delegate

    super.init()
    session = MCSession(peer: myPeerID, securityIdentity: nil, encryptionPreference: .required)
    session.delegate = self

    if (self.sessionType.rawValue & SessionType.host.rawValue) != 0 {
      serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: nil, serviceType: self.serviceName)
      serviceAdvertiser?.delegate = self
      serviceAdvertiser?.startAdvertisingPeer()
    }

    if (self.sessionType.rawValue & SessionType.peer.rawValue) != 0 {
      serviceBrowser = MCNearbyServiceBrowser(peer: myPeerID, serviceType: self.serviceName)
      serviceBrowser?.delegate = self
      serviceBrowser?.startBrowsingForPeers()
    }
  }

    /*
     NEW CONSTRUCTOR - Call startHosting and startJoining manually
     */
  public init(delegate: MultipeerHelperDelegate? = nil) {
    self.serviceName = "siemob-test"
    self.sessionType = .manual
    self.delegate = delegate
    
    super.init()
    session = MCSession(peer: myPeerID, securityIdentity: nil, encryptionPreference: .required)
    session.delegate = self
  }

    public func startHostingSession(){
        if self.sessionType != .manual {
            print("DEBUG: Tried to invoke startHostingSession in state \(self.sessionType)")
            return
        }
        
        serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: nil, serviceType: self.serviceName)
        serviceAdvertiser?.delegate = self
        serviceAdvertiser?.startAdvertisingPeer()
    }
    
    public func startJoiningSession(){
        if self.sessionType != .manual {
            print("DEBUG: Tried to invoke startHostingSession in state \(self.sessionType)")
            return
        }
        serviceBrowser = MCNearbyServiceBrowser(peer: myPeerID, serviceType: self.serviceName)
        serviceBrowser?.delegate = self
        serviceBrowser?.startBrowsingForPeers()
    }
    

  @discardableResult
  public func sendToAllPeers(_ data: Data, reliably: Bool) -> Bool {
    return sendToPeers(data, reliably: reliably, peers: connectedPeers)
  }

  @discardableResult
  public func sendToPeers(_ data: Data, reliably: Bool, peers: [MCPeerID]) -> Bool {
    guard !peers.isEmpty else { return false }
    do {
      try session.send(data, toPeers: peers, with: reliably ? .reliable : .unreliable)
    } catch {
      print("error sending data to peers \(peers): \(error.localizedDescription)")
      return false
    }
    return true
  }
}

extension MultipeerHelper: MCSessionDelegate {
  public func session(
    _: MCSession,
    peer peerID: MCPeerID,
    didChange state: MCSessionState
  ) {
    switch state {
    case .connected:
        self.connectedPeers.append(peerID)
        delegate?.peerJoined?(peerID)
        print("Connected: \(peerID.displayName)")
    case .connecting: print("Connecting: \(peerID.displayName)")
    case .notConnected:
        self.connectedPeers.removeAll(where: { $0 == peerID })
        delegate?.peerLeft?(peerID)
        print("Disconnected: \(peerID.displayName)")
    default: print("Undefined state: \(peerID.displayName)")
    }
  }

  public func session(_: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
    delegate?.receivedData?(data, peerID)
  }

  public func session(
    _: MCSession,
    didReceive stream: InputStream,
    withName streamName: String,
    fromPeer peerID: MCPeerID
  ) {
    delegate?.receivedStream?(stream, streamName, peerID)
  }

  public func session(
    _: MCSession,
    didStartReceivingResourceWithName resourceName: String,
    fromPeer peerID: MCPeerID,
    with progress: Progress
  ) {
    delegate?.receivingResource?(resourceName, peerID, progress)
  }

  public func session(
    _: MCSession,
    didFinishReceivingResourceWithName resourceName: String,
    fromPeer peerID: MCPeerID,
    at localURL: URL?,
    withError error: Error?
  ) {
    delegate?.receivedResource?(resourceName, peerID, localURL, error)
  }
}

extension MultipeerHelper: MCNearbyServiceBrowserDelegate {
  /// - Tag: SendPeerInvite
  public func browser(
    _ browser: MCNearbyServiceBrowser,
    foundPeer peerID: MCPeerID,
    withDiscoveryInfo _: [String: String]?
  ) {
    // Ask the handler whether we should invite this peer or not
    let accepted = delegate?.peerDiscovered?(peerID) ?? false
    if delegate?.peerDiscovered == nil || accepted != false {
      browser.invitePeer(peerID, to: session, withContext: nil, timeout: 10)
    }
  }

  public func browser(_: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
    delegate?.peerLost?(peerID)
  }
}

extension MultipeerHelper: MCNearbyServiceAdvertiserDelegate {
  /// - Tag: AcceptInvite
  public func advertiser(
    _: MCNearbyServiceAdvertiser,
    didReceiveInvitationFromPeer _: MCPeerID,
    withContext _: Data?,
    invitationHandler: @escaping (Bool, MCSession?) -> Void
  ) {
    // Call the handler to accept the peer's invitation to join.
    invitationHandler(true, session)
  }
}


@objc public protocol MultipeerHelperDelegate: AnyObject {
  /// Data that has been recieved from another peer
  /// - Parameters:
  ///     - data: The data which has been recieved
  ///     - peer: The peer that sent the data
  @objc optional func receivedData(_ data: Data, _ peer: MCPeerID)

  /// Callback for when a peer joins the network
  /// - Parameter peer: the   `MCPeerID` of the newly joined peer
  @objc optional func peerJoined(_ peer: MCPeerID)

  /// Callback for when a peer leaves the network
  /// - Parameter peer: the   `MCPeerID` of the peer that left
  @objc optional func peerLeft(_ peer: MCPeerID)

  /// Callback for when a peer new peer has been found. will default to accept all peers
  /// - Parameter peer: the   `MCPeerID` of the peer who wants to join the network
  /// - Returns: Bool if the peer should be invited to the network or not
  @objc optional func peerDiscovered(_ peer: MCPeerID) -> Bool

  /// Peer can no longer be found on the network, and thus cannot receive data
  /// - Parameter peer: If a peer has left the network in a non typical way
  @objc optional func peerLost(_ peer: MCPeerID)
  @objc optional func receivedStream(_ stream: InputStream, _ streamName: String, _ peer: MCPeerID)
  @objc optional func receivingResource(_ resourceName: String, _ peer: MCPeerID, _ progress: Progress)
  @objc optional func receivedResource(_ resourceName: String, _ peer: MCPeerID, _ url: URL?, _ error: Error?)
}

