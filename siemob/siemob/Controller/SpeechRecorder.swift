//
//  SpeechRecorder.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.11.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Speech

final class SpeechRecorder: ObservableObject {
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    var request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    @Published var text = ""
    @Published var isRecording = false
    
    
    func toggleRecording() {
        if isRecording {
            cancelRecording()
        } else {
            recordAndRecognizeSpeach()
        }
    }
    func cancelRecording() {
        recognitionTask?.finish()
        recognitionTask = nil
        
        // stop audio
        request.endAudio()
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        
        request = SFSpeechAudioBufferRecognitionRequest()
        
        self.isRecording = false
    }
    

    func recordAndRecognizeSpeach() {
        SFSpeechRecognizer.requestAuthorization({ _ in
            //
        })
        
        self.isRecording = true
        let node = audioEngine.inputNode
        
        if(node.outputFormat(forBus: 0).channelCount == 0){
            NSLog("Not enough available outputs!")
            self.isRecording = false
            audioEngine.reset()
            return
        }
        
        let recordingFormat = node.outputFormat(forBus: 0)
        
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            return
        }
        if !myRecognizer.isAvailable {
            return
        }
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                self.text = result.bestTranscription.formattedString
            } else if let error = error {
                print(error)
            }
       })
    }
}
