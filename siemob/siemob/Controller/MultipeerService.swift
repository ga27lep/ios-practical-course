//
//  MultipeerSession.swift
//  siemob
//
//  Created by Dávid Endrédi on 29.11.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
import MultipeerConnectivity
import SwiftUI

typealias PeerID = MCPeerID

protocol MultipeerReceiveDataHandler {
    func reveice(data: Data, from: PeerID)
}

class MultipeerService: NSObject & ObservableObject {
    
    @Published var connectedPeers = [MCPeerID]()
    
    private var receiveDataHandlers: [MultipeerReceiveDataHandler] = [MultipeerReceiveDataHandler]()
    
    public static let serviceType = "siemob-iMagine"
    private let myPeerID = MCPeerID(displayName: UIDevice.current.name)
    public var session: MCSession
    private var serviceAdvertiser: MCNearbyServiceAdvertiser
    
    override public init() {
        session = MCSession(peer: myPeerID, securityIdentity: nil, encryptionPreference: .required)
        serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: nil, serviceType: MultipeerService.serviceType)
        super.init()
        
        serviceAdvertiser.delegate = self
        session.delegate = self
    }
    
    func startHostingSession() {
        serviceAdvertiser.startAdvertisingPeer()
        
        print("MultipeerService: Started hosting session")
    }
    
    func stopHostingSession() {
        serviceAdvertiser.stopAdvertisingPeer()
    }
    
    func leaveSession() {
        session.disconnect()
    }
    
    func subscribeAs(receiveDataHandler: MultipeerReceiveDataHandler) -> MultipeerService {
        self.receiveDataHandlers.append(receiveDataHandler)
        return self
    }
    
    func sendToAllPeers(_ data: Data) {
        do {
            try session.send(data, toPeers: session.connectedPeers, with: .reliable)
        } catch {
            print("error sending data to peers: \(error.localizedDescription)")
        }
    }
}

extension MultipeerService: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            self.connectedPeers.append(peerID)
            print("Connected: \(peerID.displayName)")
        case .connecting: print("Connecting: \(peerID.displayName)")
        case .notConnected:
            self.connectedPeers.removeAll(where: { $0 == peerID })
            print("Disconnected: \(peerID.displayName)")
        default: print("Undefined state: \(peerID.displayName)")
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        //Call all subscribed dataHandlers
        for handler in self.receiveDataHandlers {
            handler.reveice(data: data, from: peerID)
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        fatalError("This service does not send/receive streams.")
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        fatalError("This service does not send/receive resources.")
    }
    
    func session(_ session: MCSession,
                 didFinishReceivingResourceWithName resourceName: String,
                 fromPeer peerID: MCPeerID,
                 at localURL: URL?,
                 withError error: Error?) {
        fatalError("This service does not send/receive resources.")
    }
}

extension MultipeerService: MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser,
                    didReceiveInvitationFromPeer peerID: MCPeerID,
                    withContext context: Data?,
                    invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        // Call handler to accept invitation and join the session.
        invitationHandler(true, self.session)
    }
}
