//
//  ServerResponse.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation
struct Token: Decodable {
    let token: String
}

struct ProjectResponse: Decodable {
    let id: String
    let name: String
}
struct AllProjectResponse: Decodable {
    struct ServerProjects: Decodable {
        let id: String
        let name: String
    }
    let projects: [ServerProjects]
}

struct StoryResponse: Decodable {
   let id: String
   let text: String
   let story: String
}
struct AllStoryResponse: Decodable {
    struct ServerStories: Decodable {
        let id: String
        let text: String
        let story: String
    }
    let stories: [ServerStories]
}
