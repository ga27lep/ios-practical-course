//
//  Persistence.swift
//  siemob
//
//  Created by Simon Dabrowski on 22.12.19.
//  Copyright © 2019 TUM. All rights reserved.
//

import Foundation


 // Enable & disable persistence through this switch.

enum Persistence {
    static let isEnabled = true
    static let tokenStoringMethod = TokenStoringMethod.userDefaults
}
